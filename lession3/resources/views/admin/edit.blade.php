
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous">
    </script>
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js"
        integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p" crossorigin="anonymous">
    </script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js"
        integrity="sha384-cVKIPhGWiC2Al4u+LWgxfKTRIcfu0JTxR+EQDz/bgldoEyl4H0zUF0QKbrJ0EcQF" crossorigin="anonymous">
    </script>

</head>


<form method = "POST" action = '{{ route("admin.update",$admin->id)}}'>
    @csrf
    @method('put')
    <div class = "container">
        <h1 class = "text-center" style="color:blue">Chỉnh sửa dữ liệu</h1>
  <div class="mb-4">
    <label for="exampleInputEmail1" class="form-label">Name</label>
    <input type="text" name="name" class="form-control" value ="{{$admin->name}} ">
   
  </div>
  <div class="mb-4">
    <label for="exampleInputPassword1" class="form-label">Class</label>
    <input type="text" name="class" class="form-control" value ="{{$admin->class}}">
  </div>
  <div class="mb-4">
    <label for="exampleInputPassword1" class="form-label">Age</label>
    <input type="text" name="age" class="form-control" value ="{{$admin->age}}">
  </div>

  <button type="submit" class="btn btn-primary">Submit</button>
</div>
</form>