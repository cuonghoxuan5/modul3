<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AdminModels extends Model
{
    use HasFactory;
    protected $table    = 'text';
    protected $fillable = ['name', 'age' ,'class'];
    public $timestamps  = false;
}
