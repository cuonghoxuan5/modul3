<?php

namespace App\Http\Controllers;
use App\Models\AdminModels;
use Illuminate\Http\Request;

class AdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */ 
    public function index()
    {
        $admins = AdminModels::all();
        return view('admin.index',compact('admins'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
        return view('admin.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request['name']);
        $admin = new AdminModels();
        $admin ->name   = $request['name'];
        $admin ->class  = $request['class'];
        $admin ->age    = $request['age'];
        $admin -> save();
        return redirect()->route('admin.index');
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $admin =AdminModels::find($id);
        $params =['admin' => $admin];
        return view('admin.show' , $params);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $admin =AdminModels::find($id);
        return view('admin.edit',['admin'=>$admin]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // dd($request);
        
        $admin = AdminModels::find($id);
        $admin ->name   = $request['name'];
        $admin ->class  = $request['class'];
        $admin ->age    = $request['age'];
        $admin -> save();
        return redirect()->route('admin.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $admin = AdminModels::find($id)->delete();
        return redirect()->route('admin.index');

    }
}
