<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AdminController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome',['name'=>'cường']);
});
Route::prefix('admin')->group(function () {
    Route::get('/index',[AdminController::class , 'index'])->name('admin.index');
    Route::get('/create',[AdminController::class , 'create'])->name('admin.create');
    Route::post('/create',[AdminController::class , 'store'])->name('admin.store');
    Route::get('/{id}/show',[AdminController::class , 'show'])->name('admin.show');
    Route::get('/{id}/edit',[AdminController::class , 'edit'])->name('admin.edit');
    Route::put('/{id}/update',[AdminController::class , 'update'])->name('admin.update');
    Route::get('/{id}/delete',[AdminController::class , 'destroy'])->name('admin.destroy');
});
