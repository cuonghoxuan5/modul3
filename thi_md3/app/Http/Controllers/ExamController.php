<?php

namespace App\Http\Controllers;

use App\Models\Exam;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class ExamController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $search = $request->search;
        
        // mục tìm kiếm 
        if ($search) {
            $note = Exam::where('name','like','%'.$search.'%')->get();
        } else {
            $note = Exam::orderBy('name','asc')->get();
        }
        $params = [
            'note' => $note
        ];

        return view('index', $params);
            
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view ('create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(exam $request)
    {
      
        Exam::create($request->only( 'group','name','birthday','phone','gender','cmnd','email','address'));
        return redirect()->route('exam.index')->with('success', 'Thêm nhân viên thành công');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $note = Exam::find($id);
        return view('index', compact('note'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $note = Exam::find($id);
        $params = [
            'note' => $note
        ];
        return view('index', $params);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(exam $request, $id)
    {
        Exam::find($id)->update($request->only('group','name','birthday','phone','gender','cmnd','email','address'));

        //tao moi xong quay ve trang danh sach khach hang
        return redirect()->route('index')->with('success', 'Cập nhật thành công.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Exam::find($id)->delete();
        return redirect()->route('exam.index')->with('success', 'Xóa thành công');
    }
}
