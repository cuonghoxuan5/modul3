<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class exam extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'group' => 'required',
            'name' => 'required',
            'gender' => 'required',
            'birthday' => 'required',
            'phone' => 'required',
            'cmnd' => 'required',
            'emal' => 'required|unique',
            'address' => 'required',
        ];
    }
}
