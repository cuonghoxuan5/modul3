
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>@yield('title')</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js" integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js" integrity="sha384-cVKIPhGWiC2Al4u+LWgxfKTRIcfu0JTxR+EQDz/bgldoEyl4H0zUF0QKbrJ0EcQF" crossorigin="anonymous"></script>

</head>

<body>
<form method = "post" action = '{{route("exam.store")}}' > 
    @csrf
    <h1 class = "text-center">Thêm Nhân Viên </h1>
    <div class="container">
        
        <div class="mb-3">
            <label for="exampleInputEmail1" class="form-label">Nhóm nhân viên: </label>
            <input type="text" class="form-control" id="exampleInputEmail1" name = "group">
            @if ($errors)
                <div class = "text-danger">{{$errors->first('group')}}</div>
            @endif
        </div>
        
        <div class="mb-3">
            <label for="exampleInputEmail1" class="form-label">Tên: </label>
            <input type="text" class="form-control" id="exampleInputEmail1" name = "name">
            @if ($errors)
                <div class = "text-danger">{{$errors->first('name')}}</div>
            @endif
        </div>

        <div class="mb-3">
            <label for="exampleInputEmail1" class="form-label">Ngày sinhh :</label>
            <input type="date" class="form-control" id="exampleInputEmail1" name = "birthday">
            @if ($errors)
                <div class = "text-danger">{{$errors->first('birthday')}}</div>
            @endif
        </div>
        
        <div class="mb-3">
            <label for="exampleInputEmail1" class="form-label"> Giới tính: </label>
           
            <select name = "gender" class="form-control">
                <option></option>
                <option> nam </option>
                <option> nữ </option>
            </select>
            @if ($errors)
                <div class = "text-danger">{{$errors->first('gender')}}</div>
            @endif
        </div>
        <div class="mb-3">
            <label for="exampleInputEmail1" class="form-label">Số điện thoại :</label>
            <input type="number" class="form-control" id="exampleInputEmail1" name = "phone">
            @if ($errors)
                <div class = "text-danger">{{$errors->first('phone')}}</div>
            @endif
        </div>
        <div class="mb-3">
            <label for="exampleInputEmail1" class="form-label">CMND:</label>
            <input type="munber" class="form-control" id="exampleInputEmail1" name = "cmnd">
            @if ($errors)
                <div class = "text-danger">{{$errors->first('cmnd')}}</div>
            @endif
        </div>
        <div class="mb-3">
            <label for="exampleInputEmail1" class="form-label">Email :</label>
            <input type="email" class="form-control" id="exampleInputEmail1" name = "email">
            @if ($errors)
                <div class = "text-danger">{{$errors->first('email')}}</div>
            @endif
        </div>
        <div class="mb-3">
            <label for="exampleInputEmail1" class="form-label">Địa chỉ :</label>
            <input type="text" class="form-control" id="exampleInputEmail1" name = "address">
            @if ($errors)
                <div class = "text-danger">{{$errors->first('address')}}</div>
            @endif
        </div>
       
  
         
        <button type="submit" class="btn btn-primary"> Thêm</button>
        
    </div>
</form>

</body>

</html>