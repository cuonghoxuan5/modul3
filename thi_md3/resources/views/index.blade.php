<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>@yield('title')</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js" integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js" integrity="sha384-cVKIPhGWiC2Al4u+LWgxfKTRIcfu0JTxR+EQDz/bgldoEyl4H0zUF0QKbrJ0EcQF" crossorigin="anonymous"></script>

</head>

<body>
<div class = "container">
<table class="table">
    <h2 class = "text-center">Thông Tin Nhân Viên</h2>
    <form method="get" action="">
            <div class="input-group">
                <input class="form-control" type="text" name="search" placeholder="Search....." />
                <button class="btn btn-primary" type="submit"><i class="fas fa-search"></i></button>
            </div>
        </form>
        <a class="btn btn-success" href="{{route('exam.create')}}">Thêm</a>
    <thead>
    <tr>
      <th scope="col">Mã Nhân Viên</th>
      <th scope="col">Nhóm Nhân Viên</th>
      <th scope="col">Họ Tên</th>
      <th scope="col">Ngày sinh</th>
      <th scope="col">Giới Tính</th>
      <th scope="col">Số Điện Thoại</th>
      <th scope="col">CMND</th>
      <th scope="col">Email</th>
      <th scope="col">Địa Chỉ</th>
      <th scope="col">Hành Động</th>
    </tr>
  </thead>
  <tbody>
    @foreach($note as $key => $value)
    <tr>
      <th scope="row">1</th>
      <td>{{$value->group}}</td>
      <td>{{$value->name}}</td>
      <td>{{$value->birthday}}</td>
      <td>{{$value->gender}}</td>
      <td>{{$value->phone}}</td>
      <td>{{$value->cmnd}}</td>
      <td>{{$value->email}}</td>
      <td>{{$value->address}}</td>
      <td>
      <a class="btn btn-info" href="{{ route('exam.edit', $value->id) }}"><i class="fas fa-edit">sửa</i></a>
          <form method="post" action="{{ route('exam.destroy', $value->id) }}"  style="display:inline">
            @csrf
            @method('delete')
            <button class="btn btn-danger" type="submit" onclick="return confirm('Bạn chắc chắn muốn xóa?')"> <i class="fas fa-trash-alt"></i>xóa</button>
          </form>
      </td>
    </tr>
   @endforeach
  </tbody>
  </div>
</table>

  </body>

</html>