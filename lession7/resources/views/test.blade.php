<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <script src = "https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script>
        $(document).ready(function(){
            $('#show').click(function(){
                var div = $("div")
                div.animate({left: '100px'}, "slow");
                div.animate({fontSize: '100px'}, "slow");
            });
        });
    </script>
</head>
<body>
    <input type = "submit" id = "show" value = "nhấp vào đây">
    <p>
        By default, all HTML elements have a static position, and cannot be moved. 
        To manipulate the position, remember to first set the CSS position property of the element to relative,
        fixed, or absolute!
    </p>
    <div style = "height:100px;width:200px;position:absolute;"> Hello </div>
</body>
</html>