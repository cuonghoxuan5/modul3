<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM"
        crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js"
        integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p"
        crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js"
        integrity="sha384-cVKIPhGWiC2Al4u+LWgxfKTRIcfu0JTxR+EQDz/bgldoEyl4H0zUF0QKbrJ0EcQF"
        crossorigin="anonymous"></script>
</head>

<body>
    <div class = "container mt-3">
        <h1 class = "text-center"> DOM Jquery </h1>
        <hr>
        <h3 id= "content" class="text-center"> Đây là nội dung trang web </h3>

        <hr>
        <br>
        <br>
        <h3> Thay đổi nội dụng trang web </h3>
        <br>
        <button class = "btn btn-primary btn-replace"> thay đổi </button>

        <br>
        <h3> Thêm nội dụng trang web </h3>
        <br>
        <button class = "btn btn-primary btn-append"> thêm </button>

        <br>
        <h3> Xóa nội dụng trang web </h3>
        <br>
        <button class = "btn btn-primary btn-remove"> xóa </button>

        <br>
        <h3> Thay đổi màu nội dụng </h3>
        <br>
        <button class = "btn btn-primary btn-style"> thay màu </button>
    </div>
    <script src = "https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js" ></script>
    <script>
        $(document).ready(function(){
            // chỉnh sửa nội dung 
            $('.btn-replace').click(function(){
                var content = "Nội dụng này đã được thay đổi";
                $('#content').text(content);
            });
            // xóa nội dung 
            var index = 1;
            $('.btn-append').click(function(){
                var update = `<br> nội dung con ${index}`;
                console.log(update);
                $('#content').append(update);
                index++;
            });
            // xóa nội dung 
            $('.btn-remove').click(function(){
                $('#content').remove();
            });
            // chỉnh sửa màu nội dung 
            $('.btn-style').click(function(){
                $('#content').css('background-color', 'red'); 
            });
        });
    </script>
</body>

</html>