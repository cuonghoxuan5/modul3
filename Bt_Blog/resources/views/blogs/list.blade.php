@extends('welcome')
@section('title' , 'Danh sách Blog')
@section ('content')

<h1 class='text-center'>Danh sách Blog</h1>
<form method='get'>
    @csrf

    <input type="text" name="search" placeholder="search....">
    <!-- <input type="submit" value="search"> -->
    <button type="submit" class="btn btn-primary"><i class="fas fa-search"></i></button>


</form>
<hr>
<a href="{{route('blogs.create')}}" class="btn btn-success"><i class="fas fa-user-plus"></i></a>
@if (Session::has('success'))
<div class="alert alert-success">{{session::get('success')}}</div>
@endif
<table class="table">
    <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">Name</th>
            <th scope="col">Content</th>
            <th scope="col">Action</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($blogs as $key => $blog)
        <tr>
            <td>{{ $key+1 }}</td>
            <td>{{ $blog->name }}</td>
            <td>{{ $blog->content }}</td>
            <td>
                <a href="{{route('blogs.edit', $blog->id) }}" class="btn btn-info"><i class="fas fa-edit"></i></a>
                <a href="{{route('blogs.delete', $blog->id) }}" class="btn btn-danger"
                    onclick="return confirm('Bạn chắc chắn muốn xóa?')"><i class="fas fa-trash-alt"></i></a>
            </td>
        </tr>
        @endforeach
    </tbody>
</table>

@endsection