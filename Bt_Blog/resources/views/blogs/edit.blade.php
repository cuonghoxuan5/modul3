@extends('welcome')
@section('title' , 'Danh sách Blog')
@section ('content')
<form method ='post' action = '{{route("blogs.update",$blog->id)}}'> 
    @csrf
    @method('put')
    <h1 style= "color:green;">Chỉnh sửa blog</h1>
  <div class="mb-3">
    <label for="exampleInputEmail1" class="form-label">Name</label>
    <input type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" name = "name" value ='{{$blog->name}}'required>
  </div>
  <div class="mb-3">
    <label for="exampleInputPassword1" class="form-label">Content</label>
    <input type="text" class="form-control" id="exampleInputPassword1" name = "content" value ='{{$blog->content}}' required>
  </div>
  
  <button type="submit" class="btn btn-primary">Edit</button>
</form>
@endsection