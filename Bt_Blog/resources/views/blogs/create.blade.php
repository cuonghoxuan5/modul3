@extends('welcome')
@section('title' , 'Thêm Blog')
@section ('content')
<form method ='post' action = '{{route("blogs.create")}}'> 
    @csrf
    <h1 style= "color:green;">Thêm blog</h1>
  <div class="mb-3">
    <label for="exampleInputEmail1" class="form-label">Name</label>
    <input type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" name = "name">
  </div>
  <div class="mb-3">
    <label for="exampleInputPassword1" class="form-label">Content</label>
    <input type="text" class="form-control" id="exampleInputPassword1" name = "content">
  </div>
  
  <button type="submit" class="btn btn-primary">Thêm</button>
  <a type="submit" class="btn btn-dark" href="{{route('blogs.index')}}" >Hủy</a>
</form>
@endsection