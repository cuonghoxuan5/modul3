<?php

namespace App\Models;
use App\Models\PhoneModel;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class IpModel extends Model
{
    use HasFactory;
    public function phone()
    {
        return $this->belongsTo(PhoneModel::class , 'phone_id');
    }
}
