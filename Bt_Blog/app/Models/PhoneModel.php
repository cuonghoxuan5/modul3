<?php

namespace App\Models;
use App\Models\IpModel;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PhoneModel extends Model
{
    use HasFactory;
    public function Ip()
    {
        return $this->hasOne(IpModel::class, 'phone_id');
    }
}
