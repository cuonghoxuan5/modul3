<?php

namespace App\Http\Controllers;

use App\Models\BlogModel;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class BlogController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $search = $request->search;
        $blogs = BlogModel::all();
        if($search)
        {
            $blogs = DB::table('blog')->where('name','like','%'.$search.'%')->get();

        }else{
            $blogs = BlogModel::all();
        }
        $params = [
            'blogs' => $blogs
        ];
        return view ('blogs.list' , $params);
        
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('blogs.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $blog           = new BlogModel();
        $blog->name     = $request['name'];
        $blog->content  = $request['content'];
        $blog->save();
        // chuyển hướng 
        return redirect()->route('blogs.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $blog =BlogModel::find($id);
        return view('blogs.edit',['blog'=>$blog]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $blog = BlogModel::find($id);
        $blog->name    = $request['name'];
        $blog->content = $request['content'];
        $blog->save();
        // chuyển hướng 
        return redirect()->route('blogs.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        BlogModel::find($id)->delete();
        return redirect()->route('blogs.index')->with('success' , 'xóa thành thành công');

    }
}
