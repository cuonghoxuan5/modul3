<?php

use App\Http\Controllers\Admin\UserController;
use App\Http\Controllers\Admin\CategoryController;
use App\Http\Controllers\Admin\OrderController;
use App\Http\Controllers\Admin\OrderdetailController;
use App\Http\Controllers\Admin\ProductController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\HomeUsersController;
use App\Http\Controllers\LoginController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('admin1', function () {
    return view('homeadmin');
});
Route::prefix('admin')->group(function () {
    Route::resource('users', UserController::class);
    Route::get('users/search', [UserController::class, 'search'])->name('users.search');

    Route::resource('products', ProductController::class);
    Route::get('products/search', [ProductController::class, 'search'])->name('products.search');

    Route::resource('categories', CategoryController::class);
    Route::get('categories/search', [CategoryController::class, 'search'])->name('categories.search');

    Route::resource('orders', OrderController::class);
    Route::get('orders/search', [OrderController::class, 'search'])->name('orders.search');

    Route::resource('orderdetails', OrderdetailController::class);
    Route::get('orderdetails/search', [OrderdetailController::class, 'search'])->name('orderdetails.search');

   
   
});


Route::prefix('/home')->group(function () {


    Route::get('/home', [HomeController::class, 'home'])->name('home');
    Route::get('/login', [HomeController::class, 'login'])->name('home.login');
    Route::get('/web', [HomeController::class, 'web'])->name('home.web');
    Route::post('/handlelogin', [HomeController::class, 'handlelogin'])->name('home.handlelogin');
    Route::get('/register', [HomeController::class, 'register'])->name('home.register');
    Route::post('/handleRegister', [HomeController::class, 'handleRegister'])->name('home.handleRegister');
    Route::get('/category/{id}', [HomeController::class, 'category'])->name('category');
    Route::get('/detail/{id}', [HomeController::class, 'detail'])->name('detail');
    Route::get('/cart', [HomeController::class, 'cart'])->name('cart');
    Route::post('/addToCart/{id}', [HomeController::class, 'addToCart'])->name('addToCart');
    Route::get('/delete/{id}', [HomeController::class, 'delete'])->name('delete');
});

Route::prefix('/homeUsers')->group(function () {
    Route::get('/category/{id}', [HomeUsersController::class, 'category'])->name('homeUsers.category');
    Route::get('/detail/{id}', [HomeUsersController::class, 'detail'])->name('homeUsers.detail');
    Route::get('/cart', [HomeUsersController::class, 'cart'])->name('homeUsers.cart');
    Route::get('/index', [HomeUsersController::class, 'index'])->name('index');
    Route::post('/addToCartUser/{id}', [HomeUsersController::class, 'addToCartUser'])->name('addToCartUser');
    Route::get('/deleteUser/{id}', [HomeUsersController::class, 'deleteUser'])->name('deleteUser');
});
