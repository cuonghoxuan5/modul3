<?php

namespace Database\Seeders;

use App\Models\OrderModel;
use Illuminate\Database\Seeder;

class OrderSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = new OrderModel();
        $users->id = '1';
        $users->users_id = 'Hồ Xuân Cường';
        $users->total = '100000';
        $users->order_date = '10/12/2015';
        $users->status = 'đã thanh toán';
        $users->save();

    }
}
