<?php

namespace Database\Seeders;

use App\Models\ProductModel;
use Illuminate\Database\Seeder;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = new ProductModel();
        $users->id = '1';
        $users->name = 'Giường Đơn';
        $users->price = '1500000';
        $users->produce = '30/10/2011';
        $users->image = 'ádadasda';
        $users->category_id = 'Giường';
        $users->save();

        $users = new ProductModel();
        $users->id = '2';
        $users->name = 'Nệm Lò Xo';
        $users->price = '1000000';
        $users->produce = '30/10/2011';
        $users->image = 'ádadsadasd';
        $users->category_id = 'Nệm';
        $users->save();
    }
}
