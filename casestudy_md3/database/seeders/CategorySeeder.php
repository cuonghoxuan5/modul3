<?php

namespace Database\Seeders;

use App\Models\CategoryModel;
use Illuminate\Database\Seeder;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = new CategoryModel();
        $users->id = '1';
        $users->name = 'Giường';
        $users->code = '100';
        $users->save();

        $users = new CategoryModel();
        $users->id = '2';
        $users->name = 'Nệm';
        $users->code = '101';
        $users->save();

        $users = new CategoryModel();
        $users->id = '2';
        $users->name = 'Bàn';
        $users->code = '101';
        $users->save();

        $users = new CategoryModel();
        $users->id = '3';
        $users->name = 'Ghế';
        $users->code = '102';
        $users->save();

        $users = new CategoryModel();
        $users->id = '3';
        $users->name = 'Tủ';
        $users->code = '103';
        $users->save();

    }
}
