<?php

namespace Database\Seeders;

use App\Models\OrderdetailModel;
use Illuminate\Database\Seeder;

class OrderdetailSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = new OrderdetailModel();
        $users->id = '1';
        $users->orders_id = 'Hồ Xuân Cường';
        $users->product_id = 'Giường Đơn';
        $users->quantity = '2';
        $users->price = '3000000';
        $users->save();
    }
}
