<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      
        $users = new User();
        $users->id = '1';
        $users->name = 'Hồ Xuân Cường';
        $users->email = 'xcuong@gmail.com';
        $users->password = '1234567a';
        $users->password_comfirm = '1234567a';
        $users->date = '03/08/2000';
        $users->gender = 'Nam';
        $users->address = 'Gio Linh, quảng trị';
        $users->phone = '0123456700';
        // $users->image = 'Phòng 503';
        // $users->status = 'Trống-Bẩn';
        $users->save();
    }
}
