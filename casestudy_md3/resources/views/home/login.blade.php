<!DOCTYPE html>

<head>
	<title>Đăng Nhập</title>
	<meta name="keywords" content="" />
	<meta name="description" content="" />
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,700" rel="stylesheet" type="text/css">
	<link href="{{ asset('layoutLogin/css/font-awesome.min.css')}}" rel="stylesheet" type="text/css">
	<link href="{{ asset('layoutLogin/css/bootstrap.min.css ')}}" rel="stylesheet" type="text/css">
	<link href="{{ asset('layoutLogin/css/bootstrap-theme.min.css')}}" rel="stylesheet" type="text/css">
	<link href="{{ asset('layoutLogin/css/bootstrap-social.css')}}" rel="stylesheet" type="text/css">
	<link href="{{ asset('layoutLogin/css/templatemo_style.css')}}" rel="stylesheet" type="text/css">
</head>

<body class="templatemo-bg-image-1">
	<div class="container">
		<div class="col-md-12">

			<form class="form-horizontal templatemo-login-form-2" role="form" action="{{route('home.handlelogin')}}" method="post">
				@csrf
				<div class="row">
					<div class="col-md-12">
						<h1>Đăng Nhập</h1>
						@if (Session::has('success'))
						<p style="width:200px" class="alert-success">
							<i class="fa fa-check" aria-hidden="true"></i>{{ Session::get('success') }}
						</p>
						@endif
					</div>
				</div>
				<div class="row">
					<div class="templatemo-one-signin col-md-6">
						<div class="form-group">
							<div class="col-md-12">
								<label for="username" class="control-label">Email</label>
								<div class="templatemo-input-icon-container">
									<i class="fa fa-user"></i>
									<input type="text" name="email" class="form-control" id="email" placeholder="">
									@if ($errors)
									<div class="text-light">{{$errors->first('email')}}</div>
									@endif
									@if (Session::has('error_email'))
									<strong class="text-light">
										<i class="fa fa-check" aria-hidden="false"></i>{{ Session::get('error_email') }}
									</strong>
									@endif
								</div>

							</div>
						</div>
						<div class="form-group">
							<div class="col-md-12">
								<label for="password" class="control-label">Mật khẩu</label>
								<div class="templatemo-input-icon-container">
									<i class="fa fa-lock"></i>
									<input type="password" name="password" placeholder class="form-control" id="password" placeholder="">
									@if ($errors)
									<div class="text-light">{{$errors->first('password')}}</div>
									@endif
									@if (Session::has('error_password'))
									<strong class="text-light">
										<i class="fa fa-check" aria-hidden="false"></i>{{ Session::get('error_password') }}
									</strong>
									@endif
								</div>
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-12">
								<div class="checkbox">
									<label>
										<input type="checkbox"> Lưu thông tin
									</label>
								</div>
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-12">
								<input type="submit" value="Đăng Nhập" class="btn btn-warning">
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-12">
								<a href="{{ route('home.register') }}" class="text-center">Chưa có tài khoản?</a>
							</div>
						</div>
					</div>
					<br><br>
					<a class="btn btn-block btn-social btn-facebook margin-bottom-15">
						<i class="fa fa-facebook"></i> Đăng nhập bằng Facebook
					</a>
					<a class="btn btn-block btn-social btn-twitter margin-bottom-15">
						<i class="fa fa-twitter"></i> Đăng nhập bằng Twitter
					</a>
					<a class="btn btn-block btn-social btn-google-plus">
						<i class="fa fa-google-plus"></i> Đăng nhập bằng Google
					</a>
				</div>
		</div>
		</form>
	</div>
	</div>
</body>

</html>