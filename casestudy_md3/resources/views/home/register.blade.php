<!DOCTYPE html>

<head>
	<!-- templatemo 418 form pack -->
	<!-- 
    Form Pack
    http://www.templatemo.com/preview/templatemo_418_form_pack 
    -->
	<title>Đăng Kí</title>
	<meta name="keywords" content="" />
	<meta name="description" content="" />
	<meta charset="UTF-8">
	<base href="{{asset('layoutLogin')}}/">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,700" rel="stylesheet" type="text/css">
	<link href="css/font-awesome.min.css" rel="stylesheet" type="text/css">
	<link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
	<link href="css/bootstrap-theme.min.css" rel="stylesheet" type="text/css">
	<link href="css/templatemo_style.css" rel="stylesheet" type="text/css">
</head>

<body class="templatemo-bg-image-2">
	<div class="container">
		<div class="col-md-12">
			<form class="form-horizontal templatemo-contact-form-1" role="form" action="{{route('home.handleRegister')}}" method="post">
				@csrf
				<div class="form-group">
					<div class="col-md-12">
						<h1 class="margin-bottom-15">Đăng Kí Ngay</h1>
						<p class="text-center">Vui lòng đăng kí để sử dụng </p>
					</div>
				</div>
				<div class="form-group">
					<div class="col-md-12">
						<label for="name" class="control-label">Name *</label>
						<div class="templatemo-input-icon-container">
							<i class="fa fa-user"></i>
							<input type="text" class="form-control" id="name"  name = "name" placeholder="">
						</div>
						@if ($errors)
						<div class="text-light">{{$errors->first('password')}}</div>
						@endif
					</div>
				</div>
				<div class="form-group">
					<div class="col-md-12">
						<label for="email" class="control-label">Email *</label>
						<div class="templatemo-input-icon-container">
							<i class="fa fa-envelope-o"></i>
							<input type="email" class="form-control" id="email" name = "email" placeholder="">
						</div>
						@if ($errors)
						<div class="text-light">{{$errors->first('password')}}</div>
						@endif
					</div>
				</div>
				<div class="form-group">
					<div class="col-md-12">
						<label for="password" class="control-label">Password *</label>
						<div class="templatemo-input-icon-container">
							<i class="fa fa-lock"></i>
							<input type="password" class="form-control" id="website" name ="password" placeholder="">
						</div>
						@if ($errors)
						<div class="text-light">{{$errors->first('password')}}</div>
						@endif
					</div>
				</div>
				<div class="form-group">
					<div class="col-md-12">
						<label for="password" class="control-label">Password Confirm *</label>
						<div class="templatemo-input-icon-container">
							<i class="fa fa-lock"></i>
							<input type="password" class="form-control" id="subject" placeholder="" name = "password_confirm">
						</div>
						@if ($errors)
						<div class="text-light">{{$errors->first('password')}}</div>
						@endif
					</div>
				</div>
				<div class="form-group">
					<div class="col-md-12">
						<label for="phone" class="control-label">Phone *</label>
						<div class="templatemo-input-icon-container">
							<i class="fa fa-phone"></i>
							<input rows="8" cols="50" class="form-control" id="message" name = "phone" placeholder="">
						</div>
						@if ($errors)
						<div class="text-light">{{$errors->first('password')}}</div>
						@endif
					</div>
				</div>
				<div class="form-group">
					<div class="col-md-12">
						<label for="home" class="control-label">Address *</label>
						<div class="templatemo-input-icon-container">
							<i class="fa fa-home"></i>
							<input rows="8" cols="50" class="form-control" id="message" name = "address" placeholder="">
						</div>
						@if ($errors)
						<div class="text-light">{{$errors->first('password')}}</div>
						@endif
					</div>
				</div>
				<div class="form-group">
					<div class="col-md-12">
						<label for="home" class="control-label">Gender *</label>
						<div class="templatemo-input-icon-container" style = "color:black">
							<select name="gender" id="" >
								<option value="nam">nam</option>
								<option value="nữ">nữ</option>
							</select>
						</div>
						@if ($errors)
						<div class="text-light">{{$errors->first('password')}}</div>
						@endif
					</div>
				</div>

				<div class="form-group">
					<div class="col-md-12">
						<label for="birthday" class="control-label">BirthDay *</label>
						<div class="templatemo-input-icon-container">
							<i class="fa fa-birthday-cake"></i>
							<input class="form-control" type="date" id="message" name="date" placeholder="">
						</div>
						@if ($errors)
						<div class="text-light">{{$errors->first('password')}}</div>
						@endif
					</div>
				</div>

				<div class="form-group">
					<div class="col-md-12">
						<input type="submit" value="Đăng Kí" class="btn btn-success pull-right">
					</div>
				</div>
			</form>
		</div>
	</div>
</body>

</html>