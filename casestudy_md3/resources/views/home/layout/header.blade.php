<!DOCTYPE html>
<html lang="en">
<!-- Basic -->

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <!-- Mobile Metas -->
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Site Metas -->
    <title>@yield('title')</title>
    <meta name="keywords" content="">
    <meta name="description" content="">
    <meta name="author" content="">
	  <base href = "{{asset('home')}}/">
    <!-- Site Icons -->
    <link rel="shortcut icon" href="{{asset('home/images/favicon.ico')}}" type="image/x-icon">
    <link rel="apple-touch-icon" href="{{asset('home/images/apple-touch-icon.png')}}">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="{{asset('home/css/bootstrap.min.css')}}">
    <!-- Site CSS -->
    <link rel="stylesheet" href="{{asset('home/css/style.css')}}">
    <!-- Responsive CSS -->
    <link rel="stylesheet" href="{{asset('home/css/responsive.css')}}">
    <!-- Custom CSS -->
    <link rel="stylesheet" href="{{asset('home/css/custom.css')}}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0-beta3/css/all.min.css" integrity="sha512-Fo3rlrZj/k7ujTnHg4CGR2D7kSs0v4LLanw2qksYuRlEzO+tcaEPQogQ0KaoGN26/zrn20ImR1DfuLWnOo7aBA==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>
<div class="main-top">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                <div class="text-slid-box">
                    <div id="offer-box" class="carouselTicker">
                        <ul class="offer-box">
                            <li>
                                <i class="fab fa-opencart"></i> Mua ngay hôm này giảm 20%
                            </li>
                            <li>
                                <i class="fab fa-opencart"></i> Cơ Hội Giảm 20%-30% Trong Hôm Nay
                            </li>
                            <li>
                                <i class="fab fa-opencart"></i> Giảm 20% Khi Nhập Mã Code : offT20
                            </li>
                            <li>
                                <i class="fab fa-opencart"></i> Miễn Phí Vận Chuyển
                            </li>
                            <li>
                                <i class="fab fa-opencart"></i> Nhanh Tay Chọn Lựa
                            </li>


                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                <div class="custom-select-box">
                    <select id="basic" class="selectpicker show-tick form-control" data-placeholder="$ USD">
                        <option>★ VND</option>
                        <option>$ USD</option>
                        <option>€ EUR</option>
                    </select>
                </div>
                <div class="right-phone-box">
                    <p>Gọi Ngay :- <a href="#"> +84921234578</a></p>
                </div>
                <div class="our-link">
                    <ul>
                        <li><a href="#">Tài Khoản Của Tôi</a></li>
                        <li><a href="{{route('index')}}">Trở lại</a></li>
                        <li><a href="#">Liên Hệ </a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End Main Top -->

<!-- Start Main Top -->
<header class="main-header">
    <!-- Start Navigation -->
    <nav class="navbar navbar-expand-lg navbar-light bg-light navbar-default bootsnav">
        <div class="container">
            <!-- Start Header Navigation -->
            <div class="navbar-header">
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar-menu" aria-controls="navbars-rs-food" aria-expanded="false" aria-label="Toggle navigation">
                    <i class="fa fa-bars"></i>
                </button>
                <a class="navbar-brand" href="index.html"><img src="images/118.png" class="logo" style="width: 115px" alt=""></a>
            </div>
            <!-- End Header Navigation -->

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="navbar-menu">
                <ul class="nav navbar-nav ml-auto" data-in="fadeInDown" data-out="fadeOutUp">
                    <li class="nav-item active"><a class="nav-link" href="{{ route('home') }}">Trang Chủ</a></li>
                    <!-- <li class="nav-item"><a class="nav-link" href="about.html">About Us</a></li> -->
                    <li class="dropdown megamenu-fw">
                        <a href="#" class="nav-link dropdown-toggle arrow" data-toggle="dropdown">Sản Phẩm</a>
                        <ul class="dropdown-menu megamenu-content" role="menu">
                            <li>
                                <div class="row">
                                    <div class="col-menu col-md-3">
                                        <h6 class="title">Giường</h6>
                                        <div class="content">
                                            <ul class="menu-col">
                                                <li><a href="{{route('category',1)}}">Giường Đơn </a></li>
                                                <li><a href="{{route('category',8)}}">Giường Đôi </a></li>
                                                <li><a href="{{route('category',9)}}">Giường Xếp </a></li>
                                                <li><a href="{{route('category',10)}}">Giường Tầng </a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <!-- end col-3 -->
                                    <div class="col-menu col-md-3">
                                        <h6 class="title">Bàn</h6>
                                        <div class="content">
                                            <ul class="menu-col">
                                                <li><a href="{{route('category',16)}}">Bàn Học </a></li>
                                                <li><a href="{{route('category',15)}}">Bàn Làm Việc </a></li>
                                                <li><a href="{{route('category',14)}}">Bàn Khách </a></li>
                                                <li><a href="{{route('category',17)}}">Bàn Ăn </a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <!-- end col-3 -->
                                    <div class="col-menu col-md-3">
                                        <h6 class="title">Ghế</h6>
                                        <div class="content">
                                            <ul class="menu-col">
                                                <li><a href="{{route('category',19)}}">Ghế Đẩu</a></li>
                                                <li><a href="{{route('category',21)}}">Ghế Sofa</a></li>
                                                <li><a href="{{route('category',18)}}">Ghế Văn Phòng</a></li>
                                                <li><a href="{{route('category',20)}}">Ghế Nhựa</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="col-menu col-md-3">
                                        <h6 class="title">Tủ</h6>
                                        <div class="content">
                                            <ul class="menu-col">
                                                <li><a href="{{route('category',5)}}">Tủ Quần Áo </a></li>
                                                <li><a href="{{route('category',12)}}">Tủ Quàn Áo Cửa Kéo </a></li>
                                                <li><a href="{{route('category',11)}}">Tủ Nhựa </a></li>
                                                <li><a href="{{route('category',13)}}">Giá Đựng Sách </a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <!-- end col-3 -->
                                </div>
                                <!-- end row -->
                            </li>
                        </ul>
                    </li>  
                    <li class="dropdown">
                        <a href="#" class="nav-link dropdown-toggle arrow" data-toggle="dropdown">SHOP</a>
                        <ul class="dropdown-menu">
                            <li><a href="{{route('cart')}}"> Giỏ Hàng </a></li>
                            <li><a href="{{route('home.login')}}"> Đăng Nhập </a></li>
                            <li><a href="{{ route('home.login')}}"> Thoát </a></li>

                        </ul>
                    </li>
                    <li class="dropdown">
                        <a href="#" class="nav-link dropdown-toggle arrow" data-toggle="dropdown">Thông Tin Liên Hệ</a>
                        <ul class="dropdown-menu">
                            <li class=""><i class='fas fa-phone'></i> 0921234578 </li>
                            <li class=""><i class='fas fa-phone'></i> 0922987345 </li>



                        </ul>
                    </li>
                    <!-- <li class="nav-item"><a class="nav-link" href="contact-us.html">Contact Us</a></li> -->
                </ul>
            </div>
            <!-- /.navbar-collapse -->

            <!-- Start Atribute Navigation -->
            <div class="attr-nav">
                <ul>
                    <li class="search"><a href="#"><i class="fa fa-search"></i></a></li>

                    <li class="side-menu"><a href="#">
                            <i class="fa fa-shopping-bag"></i>
                            <span class="badge">3</span>
                        </a></li>
                </ul>
            </div>
            <!-- End Atribute Navigation -->
        </div>
        <!-- Start Side Menu -->
        <div class="side">
            <a href="#" class="close-side"><i class="fa fa-times"></i></a>

            <li class="cart-box">
                <ul class="cart-list">
                    <li>
                        <a href="#" class="photo"><img src="images/110.jpg" class="cart-thumb" alt="" /></a>
                        <h6><a href="#">Giường Đơn </a></h6>
                        <p>1x - <span class="price">1,200,000 VND</span></p>
                    </li>
                    <li>
                        <a href="#" class="photo"><img src="images/111.jpg" class="cart-thumb" alt="" /></a>
                        <h6><a href="#">Tủ Đựng Sách </a></h6>
                        <p>1x - <span class="price">800,000 VND</span></p>
                    </li>
                    <li>
                        <a href="#" class="photo"><img src="images/112.jpg" class="cart-thumb" alt="" /></a>
                        <h6><a href="#">Ghế Văn Phòng </a></h6>
                        <p>1x - <span class="price">500,000 VND </span></p>
                    </li>
                    <li class="total">
                        <a href="#" class="btn btn-default hvr-hover btn-cart"> Giỏ Hàng</a><br>
                        <span class="float-below"><strong>Tổng</strong>:2,500,000 VND</span>
                    </li>
                </ul>
            </li>
        </div>
        <!-- End Side Menu -->
    </nav>
    <!-- End Navigation -->
</header>
<!-- End Main Top -->

<body>