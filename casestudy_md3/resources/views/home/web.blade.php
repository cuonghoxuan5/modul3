@extends('home.layout.app')
@section('content')


<!-- Start Main Top -->

<!-- Start Top Search -->
<div class="top-search">
    <div class="container">
        <div class="input-group">
            <span class="input-group-addon"><i class="fa fa-search"></i></span>
            <input type="text" class="form-control" placeholder="Search">
            <span class="input-group-addon close-search"><i class="fa fa-times"></i></span>
        </div>
    </div>
</div>
<!-- End Top Search -->

<!-- Start Slider -->
<div id="slides-shop" class="cover-slides">
    <ul class="slides-container">
        <li class="text-left">
            <img src="images/115.jpg" alt="">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <h1 class="m-b-20"><strong>Chào Mừng Đến Với <br> SpatialShop</strong></h1>
                        <p class="m-b-40">Không Gian Thoáng Mát<br> Chất Lượng Bền Bỉ.</p>
                        <p><a class="btn hvr-hover" href="#">Shop New</a></p>
                    </div>
                </div>
            </div>
        </li>
        <li class="text-center">
            <img src="images/114.jpg" alt="">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <h1 class="m-b-20"><strong>Chào Mừng Đến Với <br> SpatialShop</strong></h1>
                        <p class="m-b-40">Vui Lòng Khách Đến Vừa Lòng Khách Đi <br> Dịch Vụ Tư Vấn 24/24 </p>
                        <p><a class="btn hvr-hover" href="#">Shop New</a></p>
                    </div>
                </div>
            </div>
        </li>
        <li class="text-right">
            <img src="images/117.jpg" alt="">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <h1 class="m-b-20"><strong>Chào Mừng Đến Với <br> SpatialShop</strong></h1>
                        <p class="m-b-40"> Luôn Luôn Cho Ra Sản Phẩm Tốt Nhất Đến Với Người Dùng <br> Sản Phẩm Được Thiết Kế Tinh Xảo </p>
                        <p><a class="btn hvr-hover" href="#">Shop New</a></p>
                    </div>
                </div>
            </div>
        </li>
    </ul>
    <div class="slides-navigation">
        <a href="#" class="next"><i class="fa fa-angle-right" aria-hidden="true"></i></a>
        <a href="#" class="prev"><i class="fa fa-angle-left" aria-hidden="true"></i></a>
    </div>
</div>
<!-- End Slider -->

<!-- Start Categories  -->
<div class="categories-shop">
    <div class="container">
        <div class="row">
            @foreach ($products as $product)
            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                <div class="shop-cat-box"> 
                    <img style="width:343px ; height:300px;" class="img-fluid"  src="{{ asset('public/upload/'. $product->image) }}" alt="" />
                    <a class="btn hvr-hover" href="{{ route('detail',$product->id) }}">{{ $product->name }}</a>
                </div>
            </div>
            @endforeach

            <nav aria-label="Page navigation example">
                <ul class="pagination">
                    <span aria-hidden="true"> {{ $products->links() }}</span>
                </ul>
            </nav>
        </div>
    </div>
</div>
<!-- End Categories -->



@endsection