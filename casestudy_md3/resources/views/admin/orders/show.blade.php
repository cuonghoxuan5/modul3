@extends('layouts.admin')
@section('content')
<div class="container" style="width:600px">
    <h1 class="text-center">Chi tiết đơn hàng</h1>
    <div class="card">
        <div class="card-header" style="text-align: center;color:red">Thông tin <b style="color:black">{{ $orders->user->name}}</b></div>
        <div class="card-body">
            <div class="card-body">
                <h5 class="card-title" style="color:red">Tên : <b style="color:black"> {{ $orders->user->name }} </b> </h5>
                <p class="card-text" style="color:red"> Tổng tiền: <b style="color:black"> {{number_format($orders->total).' VNĐ'}} </b></p>
                <p class="card-text" style="color:red"> Ngày mua : <b style="color:black"> {{ $orders->order_date  }} </b></p>
                <p class="card-text" style="color:red">Trạng thái : <b style="color:black"> {{ $orders->status }} </b></p>
            </div>
            </hr>
        </div>
    </div>
    <div class="mt-2 text-end">
        <a href="{{route('orders.index')}}" class="btn btn-success"> <i class="fas fa-backward"></i> Quay lại</a>
    </div>
</div>
@endsection