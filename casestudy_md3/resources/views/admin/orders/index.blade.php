@extends('layouts.admin')

@section('content')
<h1 class="text-center">  Danh Sách Đơn Hàng </h1>

<a class="btn btn-success" href="{{route('orders.create')}}"><i class="fas fa-folder-plus"></i>Thêm</a>
@if (Session::has('success'))
<p class="text-success">
  <i class="fa fa-check" aria-hidden="true"></i>{{ Session::get('success') }}
</p>
@endif
@if (Session::has('errors'))
<p class="text-danger">
  <i class="fa fa-exclamation" aria-hidden="true"></i>{{ Session::get('errors') }}
</p>
@endif
<div>
  <table class="table table-striped">
    <thead>
      <tr>
       
        <th scope="col">#</th>
        <th scope="col"> Tên Người Mua </th>
        <th scope="col"> Ngày Mua </th>
        <th scope="col"> Tổng Tiền </th>
        <th scope="col"> Trạng Thái </th>
        <th scope="col"> Hành động </th>
      </tr>
    </thead>
    <tbody>
      @foreach ($orders as $key => $order)
      <tr>
        <td>{{ $key+1 }}</td>
        <td>{{ $order->user->name }}</td>
        <td>{{ $order->order_date }}</td>
        <td>{{ number_format($order->total) }}</td>
        <td>{{ $order->status}}</td>
        <td>
          <a class="btn btn-info" href="{{ route('orders.edit', $order->id) }}"><i class="fas fa-edit"></i></a>
          <a class="btn btn-warning" href="{{ route('orders.show', $order->id) }}"><i class="fas fa-eye"></i></a>
          <form method="post" action="{{ route('orders.destroy', $order->id) }}"  style="display:inline">
            @csrf
            @method('delete')
            <button class="btn btn-danger" type="submit" onclick="return confirm('Bạn chắc chắn muốn xóa?')"> <i class="fas fa-trash-alt"></i></button>
          </form>
        </td>
      </tr>
      @endforeach
    </tbody>
  </table>
  <nav aria-label="Page navigation example">
    <ul class="pagination">
      <span aria-hidden="true"> {{ $orders->links() }}</span>
    </ul>
  </nav>
</div>
@endsection