@extends('layouts.admin')
@section('content')

<h1 class="text-center">Chỉnh Sửa Đơn Hàng </h1>
<div class="container">
    <div class="mb-3">
        <form method="post" action='{{ route("orders.update", $order->id) }}'>
            @csrf
            @method('put')

            <label for="exampleInputEmail1" class="form-label">Tên người mua</label>
            <select name="users_id" value="{{ $order->users_id }}" class="form-control">
                @foreach ($users as $user)
                <option value="{{ $user->id }}">{{ $user->name }}</option>
                @endforeach
            </select>
            @if ($errors)
            <div class="text-danger">{{$errors->first('users_id')}}</div>
            @endif
    </div>

    <div class="mb-3">
        <label for="exampleInputEmail1" class="form-label">Ngày mua </label>
        <input type="date" class="form-control" id="exampleInputEmail1" name="order_date" value="{{ $order->order_date }}">
        @if ($errors)
        <div class="text-danger">{{$errors->first('order_date')}}</div>
        @endif
    </div>

    <div class="mb-3">
        <label for="exampleInputEmail1" class="form-label">Tổng</label>
        <input type="text" class="form-control" id="exampleInputEmail1" name="total" value="{{ $order->total }}">
        @if ($errors)
        <div class="text-danger">{{$errors->first('total')}}</div>
        @endif
    </div>
    <div class="mb-3">
        <label for="exampleInputEmail1" class="form-label">Trạng thái</label>
        <select name="status" value="{{$order->status}}" class="form-control">
            <option value=""> </option>
            <option value="Đã thanh toán"> Đã thanh toán </option>
            <option value="Chưa thanh toán"> Chưa thanh toán</option>
        </select>

        @if ($errors)
        <div class="text-danger">{{$errors->first('status')}}</div>
        @endif
    </div>


    <button type="submit" class="btn btn-primary"><i class="fas fa-pencil-alt"></i>  Chỉnh sửa</button>
    <a type="submit" class="btn btn-dark" href="{{ route('orders.index') }}"> <i class="fas fa-backward"></i> Trở lại</a>
</div>
</form>

@endsection