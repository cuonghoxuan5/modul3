@extends('layouts.admin')
@section('content')

<form method = "post" action = '{{route("orders.store")}}' > 
    @csrf
    <h1 class = "text-center">Thêm Đơn Hàng </h1>
    <div class="container">
    <div class="mb-3">
            <label for="exampleInputEmail1" class="form-label">Tên người mua :</label>
            <select name = "users_id" class="form-control">
                @foreach ($users as $user)
                <option value="{{ $user->id }}">{{ $user->name }}</option>
                @endforeach 
            </select>
            @if ($errors)
                <div class = "text-danger">{{$errors->first('users_id')}}</div>
            @endif
        </div> 
        
        <div class="mb-3">
            <label for="exampleInputEmail1" class="form-label">Tổng tiền: </label>
            <input type="number" class="form-control" id="exampleInputEmail1" name = "total">
            @if ($errors)
                <div class = "text-danger">{{$errors->first('total')}}</div>
            @endif
        </div>
      
        <div class="mb-3">
            <label for="exampleInputEmail1" class="form-label">Ngày mua :</label>
            <input type="date" class="form-control" id="exampleInputEmail1" name = "order_date">
            @if ($errors)
                <div class = "text-danger">{{$errors->first('order_date')}}</div>
            @endif
        </div>
        
        <div class="mb-3">
            <label for="exampleInputEmail1" class="form-label"> Trạng thái: </label>
            <!-- <input type="text" class="form-control" id="exampleInputEmail1" name = "status"> -->
            <select name = "status" class="form-control">
                <option></option>
                <option> Đã thanh toán </option>
                <option> Chưa thanh toán </option>
            </select>
            @if ($errors)
                <div class = "text-danger">{{$errors->first('status')}}</div>
            @endif
        </div>
  
         
        <button type="submit" class="btn btn-primary"><i class="fas fa-user-plus"></i> Thêm</button>
        <a type="submit" class="btn btn-dark" href = "{{ route('products.index') }}"><i class="fas fa-backward"></i> Trở lại</a>
    </div>
</form>

@endsection