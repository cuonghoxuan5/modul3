@extends('layouts.admin')
@section('content')

<form method = "post" action = '{{route("users.store")}}' enctype="multipart/form-data"> 
    @csrf
    <h1 class = "text-center">Thêm vào danh sách </h1>
    <div class="container">
        <div class="mb-3">
            <label for="exampleInputEmail1" class="form-label">Tên</label>
            <input type="text" class="form-control" id="exampleInputEmail1" name = "name">
            @if ($errors)
                <div class = "text-danger">{{$errors->first('name')}}</div>
            @endif
        </div>
        <div class="mb-3">
            <label for="exampleInputEmail1" class="form-label">Email</label>
            <input type="email" class="form-control" id="exampleInputEmail1" name = "email">
            @if ($errors)
                <div class = "text-danger">{{$errors->first('email')}}</div>
            @endif
        </div>
        <div class="mb-3">
            <label for="exampleInputEmail1" class="form-label"> Mật Khẩu </label>
            <input type="password" class="form-control" id="exampleInputEmail1" name = "password">
            @if ($errors)
                <div class = "text-danger">{{$errors->first('password')}}</div>
            @endif
        </div>
        <div class="mb-3">
            <label for="exampleInputEmail1" class="form-label"> Xác Nhận Mật Khẩu </label>
            <input type="password" class="form-control" id="exampleInputEmail1" name = "password_confirm">
            @if ($errors)
                <div class = "text-danger">{{$errors->first('password_confirm')}}</div>
            @endif
        </div>
        <div class="mb-3">
            <label for="exampleInputEmail1" class="form-label">Ngày tháng</label>
            <input type="date" class="form-control" id="exampleInputEmail1" name = "date">
            @if ($errors)
                <div class = "text-danger">{{$errors->first('date')}}</div>
            @endif
        </div>
        <div class="mb-3">
            <label for="exampleInputPassword1" class="form-label"> Giới Tính </label>
            <select name ="gender" >
                <option value = ""> --Chọn-- </option>
                <option value = "Nam"> Nam </option>
                <option value = "Nữ"> Nữ </option>
                
            </select>
            @if ($errors)
                <div class = "text-danger">{{$errors->first('gender')}}</div>
            @endif
        </div>
        <div class="mb-3">
            <label for="exampleInputEmail1" class="form-label">Địa chỉ</label>
            <input type=" text" class="form-control" id="exampleInputEmail1" name = "address">
            @if ($errors)
                <div class = "text-danger">{{$errors->first('address')}}</div>
            @endif
        </div>
        <div class="mb-3">
            <label for="exampleInputEmail1" class="form-label">Số điện thoại</label>
            <input type="text" class="form-control" id="exampleInputEmail1" name = "phone">
            @if ($errors)
                <div class = "text-danger">{{$errors->first('phone')}}</div>
            @endif
        </div> 
        <!-- <div class="mb-3">
            <label for="exampleInputEmail1" class="form-label">Ảnh</label>
            <input type="file" class="form-control" id="exampleInputEmail1" name = "image">
            @if ($errors)
                <div class = "text-danger">{{$errors->first('image')}}</div>
            @endif
        </div> -->
       
           
        <button type="submit" class="btn btn-primary"> <i class="fas fa-user-plus"></i> Thêm</button>
        <a type="submit" class="btn btn-dark" href = "{{ route('users.index') }}"><i class="fas fa-backward"></i>   Trở lại</a>
    </div>
</form>

@endsection