@extends('layouts.admin')
@section('content')
<div class="container">
<div class="card" >
    <div class="card-header" style = "text-align:center"><h3>Cập nhật danh sách</h3> </div>
    <div class="card-boby">
        <form method="post" action='{{route("users.update" , $user->id)}}'>
            @csrf
            @method('put')

            <label for="exampleInputEmail1" class="form-label">Tên</label>
            <input type="text" class="form-control" id="exampleInputEmail1" name = "name" value="{{ $user->name }}">
            @if ($errors)
            <div class="text-danger">{{ $errors->first('name') }}</div>
            @endif

             <label for="exampleInputEmail1" class="form-label">Email</label>
            <input type="email" class="form-control" id="exampleInputEmail1" name = "email" value="{{ $user->email }}">
            @if ($errors)
            <div class="text-danger">{{ $errors->first('email') }}</div>
            @endif
             <label for="exampleInputEmail1" class="form-label">Mật Khẩu</label>
            <input type="password" class="form-control" id="exampleInputEmail1" name = "password" value="{{ $user->password }}">
            @if ($errors)
            <div class="text-danger">{{ $errors->first('password') }}</div>
            @endif
             <label for="exampleInputEmail1" class="form-label">Xác nhận mật khẩu</label>
            <input type="password" class="form-control" id="exampleInputEmail1" name = "password_confirm" value="{{ $user->password }}">
            @if ($errors)
            <div class="text-danger">{{ $errors->first('password_confirm') }}</div>
            @endif

            <label for="exampleInputEmail1" class="form-label">Ngày Tháng</label>
            <input type="date" class="form-control" id="exampleInputEmail1" name = "date" value="{{ $user->date }}">
            @if ($errors)
            <div class="text-danger">{{ $errors->first('date') }}</div>
            @endif

            <label for="exampleInputPassword1" class="form-label"> Giới tính </label>
            <select class = "form-control" value="{{ $user->gender }}" name = "gender" >
                <option value=""> --Chọn-- </option>
                <option value="Nam"> Nam </option>
                <option value=" Nữ "> Nữ </option>
            </select>
            @if ($errors)
            <div class="text-danger">{{ $errors->first('gender') }}</div>
            @endif

            <label for="exampleInputEmail1" class="form-label">Địa Chỉ</label>
            <input type="text" class="form-control" id="exampleInputEmail1" name = "address" value="{{ $user->address }}">
            @if ($errors)
            <div class="text-danger">{{ $errors->first('address') }}</div>
            @endif

            <label for="exampleInputEmail1" class="form-label">Số Điện Thoại </label>
            <input type="number" class="form-control" id="exampleInputEmail1" name = "phone" value="{{ $user->phone }}">
            @if ($errors)
            <div class="text-danger">{{ $errors->first('phone') }}</div>
            @endif 

            <!-- <label for="exampleInputEmail1" class="form-label"> Ảnh </label>
            <input type="file" class="form-control" id="exampleInputEmail1" name = "image" value="{{ $user->image }}">
            @if ($errors)
            <div class="text-danger">{{ $errors->first('image') }}</div>
            @endif  -->

            <button type="submit" class="btn btn-primary"><i class="fas fa-pencil-alt"></i> Cập nhật </button>
            <a type="submit" class="btn btn-dark" href = "{{ route('users.index') }}"> <i class="fas fa-backward"></i> Quay lại </a>
        </form>

    </div>
</div>
</div>


@endsection