@extends('layouts.admin')
@section('content')
<div class="container" style="width:600px">
    <h1 class= "text-center">Chi tiết danh sách </h1>
    <div class="card">
        <div class="card-header" style="text-align: center;color:red">Thông tin <b style="color:black">{{ $user->name }}</b></div>
        <div class="card-body">
            <div class="card-body">
                <h5 class="card-title" style="color:red">Tên      : <b style="color:black"> {{ $user->name }} </b> </h5>
                <!-- <p class="card-text" style="color:red">Trạng Thái : <b style="color:black"> {{ $user->status }} </b></p> -->
                <p class="card-text" style="color:red">Email      : <b style="color:black"> {{ $user->email }} </b></p>
                
                <p class="card-text" style="color:red">Ngày tháng : <b style="color:black"> {{ $user->date }} </b></p>
                <p class="card-text" style="color:red">Giới tính  : <b style="color:black"> {{ $user->gender }} </b></p>
                <p class="card-text" style="color:red">Địa chỉ    : <b style="color:black"> {{ $user->address }} </b></p>
                <p class="card-text" style="color:red">Số điện thoại : <b style="color:black"> {{ $user->phone }} </b></p> 
                <!-- <p class="card-text" style="color:red">Ảnh : <b style="color:black">  <img src="{{ asset('public/upload/'.$user->image) }}" alt="" style="width: 150px"> </b></p> -->
            </div>
            </hr>
        </div>
    </div>
    <div class= "mt-2 text-end">
    <a href="{{route('users.index')}}" class="btn btn-success"> <i class="fas fa-backward"></i> Quay lại</a>
    </div>
</div>
@endsection 