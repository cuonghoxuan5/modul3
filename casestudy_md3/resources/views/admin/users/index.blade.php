
@extends('layouts.admin')
@section('content')
<h1 class="text-center"> Quản Lí Danh Sách Khách Hàng </h1>

<a class="btn btn-success" href="{{route('users.create')}}"><i class="fas fa-folder-plus"></i>Thêm  </a>
@if (Session::has('success'))
<p class="text-success">
  <i class="fa fa-check" aria-hidden="true"></i>{{ Session::get('success') }}
</p>
@endif
@if (Session::has('errors'))
<p class="text-danger">
  <i class="fa fa-exclamation" aria-hidden="true"></i>{{ Session::get('errors') }}
</p>
@endif
<div>
  <table class="table table-striped">
    <thead>
      <tr>
        <th scope="col">#</th>
        <th scope="col"> Tên  </th>
        <th scope="col"> Email </th>
        <th scope="col"> Ngày tháng </th>
        <th scope="col"> Giới tính </th>
        <th scope="col"> Địa chỉ </th>
        <th scope="col"> Số điện thoại </th> 
        <th scope="col"> Hành động </th>
       
      </tr>
    </thead>
    <tbody>
      @foreach ($users as $key => $user)
      <tr>
        <td>{{ $key+1 }}</td>
        <td>{{ $user->name }}</td>
        <td>{{ $user->email }}</td>
        <td>{{ $user->date }}</td>
        <td>{{ $user->gender }}</td>
        <td>{{ $user->address }}</td>
        <td>{{ $user->phone }}</td>
        <!-- <td><img src="{{ asset('public/upload/'.$user->image) }}" alt="" style="width: 90px"></td>  -->
     
        <td>
          <a class="btn btn-info" href="{{ route('users.edit', $user->id) }}"><i class="fas fa-edit"></i></a>
          <a class="btn btn-warning" href="{{ route('users.show', $user->id) }}"><i class="fas fa-eye"></i></a>
          <form method="post" action="{{ route('users.destroy', $user->id) }}" style="display:inline">
            @csrf
            @method('delete')
            <button class="btn btn-danger" type="submit" onclick="return confirm('Bạn chắc chắn muốn xóa?')"> <i class="fas fa-trash-alt"></i></button>
          </form>
        </td>
      </tr>
      @endforeach
    </tbody>
  </table>
  <nav aria-label="Page navigation example">
    <ul class="pagination">
      <span aria-hidden="true"> {{ $users->links() }}</span>
    </ul>
  </nav>
</div>
@endsection