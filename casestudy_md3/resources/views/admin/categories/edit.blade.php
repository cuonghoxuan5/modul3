@extends('layouts.admin')
@section('content')

<h1 class="text-center">Chỉnh Sửa Đơn Hàng </h1>
<div class="container">
    <div class="mb-3">
        <form method="post" action='{{ route("categories.update", $categories->id) }}'>
            @csrf
            @method('put')
            <label for="exampleInputEmail1" class="form-label">Tên</label>
            <input type="text" class="form-control" id="exampleInputEmail1" name="name" value="{{ $categories->name }}">
            @if ($errors)
            <div class="text-danger">{{$errors->first('name')}}</div>
            @endif
    </div>

    <div class="mb-3">
        <label for="exampleInputEmail1" class="form-label">Mã sản Phẩm </label>
        <input type="number" class="form-control" id="exampleInputEmail1" name="code" value="{{ $categories->code }}">
        @if ($errors)
        <div class="text-danger">{{$errors->first('code')}}</div>
        @endif
    </div>

  
    <button type="submit" class="btn btn-primary"><i class="fas fa-pencil-alt"></i> Chỉnh sửa</button>
    <a type="submit" class="btn btn-dark" href="{{ route('categories.index') }}"> <i class="fas fa-backward"></i> Trở lại</a>
</div>
</form>

@endsection