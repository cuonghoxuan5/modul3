@extends('layouts.admin')
@section('content')

<form method = "post" action = '{{route("categories.store")}}' > 
    @csrf
    <h1 class = "text-center">Thêm Loại Sản Phẩm </h1>
    <div class="container">
        <div class="mb-3">
            <label for="exampleInputEmail1" class="form-label">Tên</label>
            <input type="text" class="form-control" id="exampleInputEmail1" name = "name">
            @if ($errors)
                <div class = "text-danger">{{$errors->first('name')}}</div>
            @endif
        </div>
        
        <div class="mb-3">
            <label for="exampleInputEmail1" class="form-label">Mã Sản Phẩm</label>
            <input type="number" class="form-control" id="exampleInputEmail1" name = "code">
            @if ($errors)
                <div class = "text-danger">{{$errors->first('code')}}</div>
            @endif
        </div>
       
        <button type="submit" class="btn btn-primary"><i class="fas fa-user-plus"></i>  Thêm</button>
        <a type="submit" class="btn btn-dark" href = "{{ route('categories.index') }}"><i class="fas fa-backward"></i> Trở lại</a>
    </div>
</form>

@endsection