@extends('layouts.admin')
@section('content')
<div class="container" style="width:600px">
    <h1 class= "text-center">Chi tiết danh sách </h1>
    <div class="card">
        <div class="card-header" style="text-align: center;color:red">Thông tin <b style="color:black">{{ $categories->name }}</b></div>
        <div class="card-body">
            <div class="card-body">
                <h5 class="card-title" style="color:red">Tên      : <b style="color:black"> {{ $categories->name }} </b> </h5>
                <p class="card-text" style="color:red">Mã sản phẩm : <b style="color:black"> {{ $categories->code }} </b></p>
           
            </div>
            </hr>
        </div>
    </div>
    <div class= "mt-2 text-end">
    <a href="{{route('categories.index')}}" class="btn btn-success"> <i class="fas fa-backward"></i> Quay lại</a>
    </div>
</div>
@endsection 