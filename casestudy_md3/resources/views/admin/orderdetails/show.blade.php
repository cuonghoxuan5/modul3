@extends('layouts.admin')
@section('content')
<div class="container" style="width:600px">
    <h1 class= "text-center">Chi tiết danh sách </h1>
    <div class="card">
        <div class="card-header" style="text-align: center;color:red">Thông tin <b style="color:black">{{ $orderdetails->order->user->name   }}</b></div>
        <div class="card-body">
            <div class="card-body">
                <h5 class="card-title" style="color:red">Tên         : <b style="color:black"> {{ $orderdetails ->order->user->name }} </b> </h5>
                <p class="card-text" style="color:red">Tên Sản Phẩm : <b style="color:black"> {{ $orderdetails->product->name }} </b></p>
                <p class="card-text" style="color:red">Số Lượng     : <b style="color:black"> {{ $orderdetails->quantity }} </b></p>
                <p class="card-text" style="color:red">Giá Tiền     : <b style="color:black"> {{ number_format($orderdetails->price).' VNĐ' }} </b></p>
              
            </div>
            </hr>
        </div>
    </div>
    <div class= "mt-2 text-end">
    <a href="{{route('orderdetails.index')}}" class="btn btn-success"> <i class="fas fa-backward"></i> Quay lại</a>
    </div>
</div>
@endsection 