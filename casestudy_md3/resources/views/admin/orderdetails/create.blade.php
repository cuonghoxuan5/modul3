@extends('layouts.admin')
@section('content')

<form method="post" action='{{ route("orderdetails.store") }}'>
    @csrf
    <h1 class="text-center">Thêm Chi Tiết Đơn Hàng </h1>
    <div class="container">

        <div class="mb-3">
            <label for="exampleInputPassword1" class="form-label">Tên Người Mua </label>
            <select name="orders_id" class="form-control">
                @foreach($orders as $order)
                <option value="{{ $order->id }}"> {{ $order->user->name }} </option>
                @endforeach
            </select>
            @if ($errors)
            <div class="text-danger">{{$errors->first('orders_id')}}</div>
            @endif
        </div>
        <div class="mb-3">
            <label for="exampleInputPassword1" class="form-label">Tên Sản Phẩm </label>
            <select name="products_id" class="form-control">
                @foreach($products as $product)
                <option value="{{ $product->id }}"> {{ $product->name }} </option>
                @endforeach
            </select>
            @if ($errors)
            <div class="text-danger">{{$errors->first('products_id')}}</div>
            @endif
        </div>
        <div class="mb-3">
            <label for="exampleInputEmail1" class="form-label">Số Lượng</label>
            <input type="number" class="form-control" id="exampleInputEmail1" name="quantity">
            @if ($errors)
            <div class="text-danger">{{$errors->first('quantity')}}</div>
            @endif
        </div>
        <div class="mb-3">
            <label for="exampleInputEmail1" class="form-label">Giá Tiền</label>
            <input type="number" class="form-control" id="exampleInputEmail1" name="price">
            @if ($errors)
            <div class="text-danger">{{$errors->first('price')}}</div>
            @endif
        </div>
        <button type="submit" class="btn btn-primary"><i class="fas fa-user-plus"></i> Thêm</button>
        <a type="submit" class="btn btn-dark" href="{{ route('orderdetails.index') }}"><i class="fas fa-backward"></i> Trở lại</a>
    </div>
</form>

@endsection