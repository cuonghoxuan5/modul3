@extends('layouts.admin')

@section('content')
<h1 class="text-center"> Chi Tiết Đơn Hàng </h1>

<a class="btn btn-success" href="{{route('orderdetails.create')}}"><i class="fas fa-folder-plus"></i>Thêm</a>
@if (Session::has('success'))
<p class="text-success">
  <i class="fa fa-check" aria-hidden="true"></i>{{ Session::get('success') }}
</p>
@endif
@if (Session::has('errors'))
<p class="text-danger">
  <i class="fa fa-exclamation" aria-hidden="true"></i>{{ Session::get('errors') }}
</p>
@endif
<div>
  <table class="table table-striped">
    <thead>
      <tr>
        <th scope="col">#</th>
        <th scope="col"> Tên Người Mua </th>
        <th scope="col"> Tên Sản Phẩm </th>
        <th scope="col"> Số Lượng </th>
        <th scope="col"> Giá Tiền </th>
      </tr>
    </thead>
    <tbody>
      @foreach ($orderdetails as $key => $orderdetail)
      <tr>
        <td>{{ $key+1 }}</td>
        <td>{{ $orderdetail->order->user->name }}</td>
        <td>{{ $orderdetail->product->name }}</td>
        <td>{{ $orderdetail->quantity}}</td>
        <td>{{ number_format($orderdetail->price) }}</td>
        <td>
          <a class="btn btn-info" href="{{ route('orderdetails.edit', $orderdetail->id) }}"><i class="fas fa-edit"></i></a>
          <a class="btn btn-warning" href="{{ route('orderdetails.show', $orderdetail->id) }}"><i class="fas fa-eye"></i></a>
          <form method="post" action="{{ route('orderdetails.destroy', $orderdetail->id) }}" style="display:inline">
            @csrf
            @method('delete')
            <button class="btn btn-danger" type="submit" onclick="return confirm('Bạn chắc chắn muốn xóa?')"> <i class="fas fa-trash-alt"></i></button>
          </form>
        </td>
      </tr>
      @endforeach
    </tbody>
  </table>
  <nav aria-label="Page navigation example">
    <ul class="pagination">
      <span aria-hidden="true"> {{ $orderdetails->links() }}</span>
    </ul>
  </nav>
</div>
@endsection