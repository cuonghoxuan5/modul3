@extends('layouts.admin')
@section('content')

<form method="post" action='{{ route("products.update" ,$products->id) }}' enctype="multipart/form-data">
    @csrf
    @method('put')
    <h1 class="text-center">Chỉnh Sửa Sản Phẩm </h1>
    <div class="container">
        <div class="mb-3">
            <label for="exampleInputEmail1" class="form-label">Tên</label>
            <input type="text" class="form-control" id="exampleInputEmail1" name="name" value='{{ $products->name }}'>
            @if ($errors)
            <div class="text-danger">{{$errors->first('name')}}</div>
            @endif
        </div>
        <div class="mb-3">
            <label for="exampleInputEmail1" class="form-label">Giá</label>
            <input type="number" class="form-control" id="exampleInputEmail1" name="price" value='{{ $products->price }}'>
            @if ($errors)
            <div class="text-danger">{{$errors->first('price')}}</div>
            @endif
        </div>
        <div class="mb-3">
            <label for="exampleInputEmail1" class="form-label">Ngày Sản Xuất</label>
            <input type="date" class="form-control" id="exampleInputEmail1" name="produce" value='{{ $products->produce }}'>
            @if ($errors)
            <div class="text-danger">{{$errors->first('produce')}}</div>
            @endif
        </div>

        <div class="mb-3">
            <label for="exampleInputEmail1" class="form-label">Ảnh</label>
            <img src="{{ asset('public/upload/'.$products->image)}}" width="100px"> <br> <br>
            <input type="file" name="image" id="upload_file_input" value="{{ asset('public/upload/'.$products->image) }}">
            @if ($errors)
            <div class="text-danger">{{$errors->first('image')}}</div>
            @endif
        </div>
        <div class="mb-3">
            <label for="exampleInputEmail1" class="form-label"> Tên Loại Sản Phẩm </label>
            <select  name="category_id" value='{{ $products->category_id }}' class="form-control">
                @foreach ($categories as $category)
                <option value="{{ $category->id }}">{{ $category->name }}</option>
                @endforeach
            </select>
            @if ($errors)
            <div class="text-danger">{{$errors->first('category_id')}}</div>
            @endif
        </div>

    </div>
    <button type="submit" class="btn btn-primary"><i class="fas fa-pencil-alt"></i> Chỉnh Sửa</button>
    <a type="submit" class="btn btn-dark" href="{{ route('products.index') }}"> <i class="fas fa-backward"></i> Trở lại</a>
    </div>
</form>

@endsection