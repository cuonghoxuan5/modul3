@extends('layouts.admin')
@section('content')
<div class="container" style="width:600px">
    <h1 class= "text-center">Chi tiết danh sách </h1>
    <div class="card">
        <div class="card-header" style="text-align: center;color:red">Thông tin <b style="color:black">{{ $products->name }}</b></div>
        <div class="card-body">
            <div class="card-body">
                <h5 class="card-title" style="color:red">Tên      :         <b style="color:black"> {{ $products->name }} </b> </h5>
                <p class="card-text" style="color:red"> Giá  :              <b style="color:black"> {{ number_format($products->price).' VNĐ' }} </b></p>
                <p class="card-text" style="color:red">Ngày Sản Xuất    :   <b style="color:black"> {{ $products->produce }} </b></p>
                <p class="card-text" style="color:red">Ảnh :                <b style="color:black">  <img src="{{ asset('public/upload/'.$products->image) }}" alt="" style="width: 150px"> </b></p>
                <p class="card-text" style="color:red">Tên Loại Sản Phẩm :  <b style="color:black"> {{ $products->categories->name}} </b></p> 
            </div>
            </hr>
        </div>
    </div>
    <div class= "mt-2 text-end">
    <a href="{{route('products.index')}}" class="btn btn-success"> <i class="fas fa-backward"></i> Quay lại</a>
    </div>
</div>
@endsection 