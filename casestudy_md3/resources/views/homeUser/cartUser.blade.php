@extends('homeUser.layout.app')
@section('content')
<!-- Start Cart  -->
<div class="cart-box-main">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="table-main table-responsive">
                    <table class="table">

                        <thead>
                            <tr class="text-center">
                                <th>Ảnh</th>
                                <th>Tên</th>
                                <th>Giá</th>
                                <th> Số lượng </th>
                                <th> Tổng tiền</th>
                                <th> Xóa </th>
                            </tr>
                        </thead>

                        <tbody>
                            @if(count($carts) === 0)
                            <tr>
                                <td colspan="6"> Chưa thêm sản phẩm vào giỏ hàng</td>
                            </tr>
                            @else
                            @foreach($carts as $cart )
                            <tr>
                                <td class="thumbnail-img">
                                    <a href="#">
                                        <img class="img-fluid" src="{{ asset('public/upload/'. $cart->image) }}" alt="" />
                                    </a>
                                </td>
                                <td class="name-pr">
                                    <a href="#">
                                        {{$cart->name}}
                                    </a>
                                </td>
                                <td class="price-pr">
                                    <p>{{number_format($cart->price).' VNĐ'}}</p>
                                </td>
                                <td class="quantity-box"><input type="number" size="4" value="{{$cart->quantity}}" min="0" step="1" class="c-input-text qty text"></td>
                                <td class="total-pr">
                                    <p>{{number_format($cart->totalPrice).' VNĐ'}}</p>
                                </td>
                                <td class="remove-pr">
                                    <a href="{{route('deleteUser',$cart->id)}}">
                                        <i class="fas fa-times"></i>
                                    </a>
                                </td>
                            </tr>
                            @endforeach
                            @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

        <div class="row my-5">
            <div class="col-lg-6 col-sm-6">
                <div class="coupon-box">
                   
                </div>
            </div>
            <div class="col-lg-6 col-sm-6">
                <div class="update-box">
                    <input value="Thanh Toán" type="submit">
                </div>
            </div>
        </div>

        <div class="row my-5">
            <div class="col-lg-8 col-sm-12"></div>
            <div class="col-lg-4 col-sm-12">
                <div class="order-box">
                    <!-- <h3>Tổng thanh toán</h3> -->
                    <div class="d-flex">
                        <h3>Tổng số lượng</h3>
                        @if(!$totalQuantity)
                        <div class="ml-auto font-weight-bold">Chưa nhận số lượng </div>
                        @else
                        <div class="ml-auto font-weight-bold">{{$totalQuantity->totalQuantity}} </div>
                        @endif
                    </div>

                    <div class="d-flex gr-total">
                        <h5>Tổng tiền </h5>
                        @if(!$totalPrice )
                        <div class="ml-auto font-weight-bold">Chưa nhận sản phẩm </div>
                        @else
                        <div class="ml-auto font-weight-bold">{{number_format($totalPrice->totalPrice).' VNĐ'}}</div>
                        @endif
                    </div>
                    <hr>
                </div>
            </div>
            <div class="col-12 d-flex shopping-box"><a onclick="return confirm('Mua thành Công')" href="{{route('index')}}" class="ml-auto btn hvr-hover">Mua</a> </div>
        </div>

    </div>
</div>
<!-- End Cart -->

@endsection