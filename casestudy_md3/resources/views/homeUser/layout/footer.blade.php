 <!-- Start Footer  -->
 <footer>
        <div class="footer-main">
            <div class="container">
                <div class="row">
                    <div class="col-lg-4 col-md-12 col-sm-12">
                        <div class="footer-widget">
                            <h4>Giới Thiệu Về Shop  </h4>
                            <p>Chào mừng bạn đến với shop của chúng tôi .Chúng tôi sẽ hỗ trợ và giới thiệu những sản phẩm tốt đến với khách hàng
                                </p>
                            <ul>
                                <li><a href="#"><i class="fab fa-facebook" aria-hidden="true"></i></a></li>
                                <li><a href="#"><i class="fab fa-twitter" aria-hidden="true"></i></a></li>
                                <li><a href="#"><i class="fab fa-linkedin" aria-hidden="true"></i></a></li>
                                <li><a href="#"><i class="fab fa-google-plus" aria-hidden="true"></i></a></li>
                                <li><a href="#"><i class="fa fa-rss" aria-hidden="true"></i></a></li>
                                <li><a href="#"><i class="fab fa-pinterest-p" aria-hidden="true"></i></a></li>
                                <li><a href="#"><i class="fab fa-whatsapp" aria-hidden="true"></i></a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-12 col-sm-12">
                        <div class="footer-link">
                            <h4>Thông Tin</h4>
                            <ul>
                                <li><a href="#">Liên Hệ Chúng Tôi </a></li>
                                <li><a href="#">Dịch Vụ Khách Hàng</a></li>
                                <li><a href="#">Thông Tin Giao Hàng</a></li>
                                <li><a href="#">Điều Khoản &amp; Điều Kiện</a></li>
                                <li><a href="#">Chính Sách Bảo Mật</a></li>
                                <li><a href="{{route('home.login')}}">Đăng Xuất</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-12 col-sm-12">
                        <div class="footer-link-contact">
                            <h4>Liên Hệ Với Chúng Tôi</h4>
                            <ul>
                                <li>
                                    <p><i class="fas fa-map-marker-alt"></i>Địa Chỉ : 133 Lý Thường Kiệt- Đông Hà - Quảng Trị </p>
                                </li>
                                <li>
                                    <p><i class="fas fa-phone-square"></i>SĐT: <a href="tel:+1-888705770">+84921234578</a></p>
                                </li>
                                <li>
                                    <p><i class="fas fa-envelope"></i>Email: <a href="mailto:contactinfo@gmail.com">XuanCuong@gmail.com</a></p>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <!-- End Footer  -->

    <!-- Start copyright  -->
    <div class="footer-copyright">
        <p class="footer-company">Đăng Kí Bản Quyền. &copy; 2018 <a href="{{route('home.web')}}">Gia Phát Shop</a> Thiết Kế Bởi : Xuân Cường 
            <a href="https://html.design/">html design</a></p>
    </div>
    <!-- End copyright  -->

    <a href="#" id="back-to-top" title="Back to top" style="display: none;">&uarr;</a>

    <!-- ALL JS FILES -->
    <script src="{{asset('home/js/jquery-3.2.1.min.js')}}"></script>
    <script src="{{asset('home/js/popper.min.js')}}"></script>
    <script src="{{asset('home/js/bootstrap.min.js')}}"></script>
    <!-- ALL PLUGINS -->
    <script src="{{asset('home/js/jquery.superslides.min.js')}}"></script>
    <script src="{{asset('home/js/jquery.nicescroll.min.js')}}"></script>
    <script src="{{asset('home/js/bootstrap-select.js')}}"></script>
    <script src="{{asset('home/js/inewsticker.js')}}"></script>
    <script src="{{asset('home/js/bootsnav.js')}}"></script>
    <script src="{{asset('home/js/images-loded.min.js')}}"></script>
    <script src="{{asset('home/js/isotope.min.js')}}"></script>
    <script src="{{asset('home/js/owl.carousel.min.js')}}"></script>
    <script src="{{asset('home/js/baguetteBox.min.js')}}"></script>
    <script src="{{asset('home/js/form-validator.min.js')}}"></script>
    <script src="{{asset('home/js/contact-form-script.js')}}"></script>
    <script src="{{asset('home/js/custom.js')}}"></script>
</body>

</html>