@extends('homeUser.layout.app')
@section('content')
<!-- Start Shop Detail  -->
<div class="shop-detail-box-main">
    <div class="container">
        <div class="row">
            <div class="col-xl-5 col-lg-5 col-md-6">
                <div id="carousel-example-1" class="single-product-slider carousel slide" data-ride="carousel">
                    <div class="carousel-inner" role="listbox">

                        <div class="carousel-item active"> <img class="d-block w-100" src="{{ asset('public/upload/'. $products->image) }}" alt="First slide"> </div>

                    </div>

                </div>
            </div>
            <div class="col-xl-7 col-lg-7 col-md-6">
                <div class="single-product-details">
                    <h2>{{$products->name}}</h2>
                    <h5>Giá Tiền : {{ number_format($products->price).' VND' }}</h5>

                    <p>
                    <ul>
                        <li>
                            <div class="form-group quantity-box">
                                <label class="control-label">Số Lượng </label>
                                <form action="{{route('addToCartUser',$products->id)}}" method="post">
                                    @csrf
                                    <input class="form-control" name="quantity" value="1" n="0" max="20" type="number">
                            </div>
                        </li>
                    </ul>
                    <div class="price-box-bar">
                        <div class="cart-and-bay-btn">
                            <a class="btn hvr-hover" data-fancybox-close="" href="#">Mua Ngay</a>
                            <button class="btn hvr-hover" data-fancybox-close="" >Thêm Giỏ Hàng</button>
                            </form>
                        </div>
                    </div>

                    <div class="add-to-btn">
                        <div class="add-comp">
                            <a class="btn hvr-hover" href="#"><i class="fas fa-heart"></i> Thêm Vào Danh Sách Yêu Thích</a>

                        </div>
                        <div class="share-bar">
                            <a class="btn hvr-hover" href="#"><i class="fab fa-facebook" aria-hidden="true"></i></a>
                            <a class="btn hvr-hover" href="#"><i class="fab fa-google-plus" aria-hidden="true"></i></a>
                            <a class="btn hvr-hover" href="#"><i class="fab fa-twitter" aria-hidden="true"></i></a>
                            <a class="btn hvr-hover" href="#"><i class="fab fa-pinterest-p" aria-hidden="true"></i></a>
                            <a class="btn hvr-hover" href="#"><i class="fab fa-whatsapp" aria-hidden="true"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row my-5">
            <div class="col-lg-12">
                <div class="title-all text-center">
                    <h1>Sản Phẩm Nổi Bật</h1>
                    <p>Sản Phẩm Cao Cấp Chất Lượng Tốt </p>
                </div>
                <div class="featured-products-box owl-carousel owl-theme">
                    @foreach($highlights as $highlight)
                    <div class="item">
                        <div class="products-single fix">
                            <div class="box-img-hover">
                                <img style="width:343px ; height:300px;" src="{{ asset('public/upload/'. $highlight->image) }}" class="img-fluid" alt="Image">
                                <div class="mask-icon">
                                    <ul>
                                        <li><a href="#" data-toggle="tooltip" data-placement="right" title="View"><i class="fas fa-eye"></i></a></li>
                                        <li><a href="#" data-toggle="tooltip" data-placement="right" title="Compare"><i class="fas fa-sync-alt"></i></a></li>
                                        <li><a href="#" data-toggle="tooltip" data-placement="right" title="Add to Wishlist"><i class="far fa-heart"></i></a></li>
                                    </ul>
                                    <a class="cart" href="{{route('addToCart',$products->id )}}">Thêm Giỏ Hàng </a>
                                </div>
                            </div>
                            <div class="why-text">
                                <h4>{{$highlight->name}}</h4>
                                <h5>{{number_format($highlight->price).' VNĐ'}}</h5>
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>

    </div>
</div>
<!-- End Cart -->

<!-- Start Instagram Feed  -->
<div class="instagram-box"> -->
    <div class="main-instagram owl-carousel owl-theme">
        @foreach ( $new_products as $new_product )
        <div class="item">
            <div class="ins-inner-box">
                <img style="width:250px ; height:200px;" src="{{ asset('public/upload/'. $new_product->image) }}" alt="" />
                <div class="hov-in">
                    <a href="#"><i class="fab fa-instagram"></i></a>
                </div>
            </div>
        </div>
        @endforeach
    </div>
</div>
<!-- End Instagram Feed  -->
@endsection