<?php

use Illuminate\Support\Facades\Request;

  $routeName = Request::route()->getName();
  $user_active = ($routeName == 'users.index') ? ' text-dark' : '';
  $order_active = ($routeName == 'orders.index') ? ' text-dark' : '';
  $product_active = ($routeName == 'products.index') ? ' text-dark' : '';
  $category_active = ($routeName == 'categories.index') ? ' text-dark' : '';
  $orderdetail_active = ($routeName == 'orderdetails.index') ? ' text-dark' : '';
//   dd($routeName);
?>
<div id="layoutSidenav_nav">
                <nav class="sb-sidenav accordion sb-sidenav-blue" id="sidenavAccordion" style="background-color:#E6F1D8 !important">
                    <div class="sb-sidenav-menu">
                        <div class="nav">
                            <div class="sb-sidenav-menu-heading">Quản lí</div>
                            <a class="nav-link" href="{{ route('users.index') }}">  
                                <div  class="sb-nav-link-icon"><i class="fas fa-home"></i></div>
                                Trang Chủ
                            </a>
                            <a class="nav-link <?= $user_active; ?>" href="{{ route('users.index') }}">
                                <div  class="sb-nav-link-icon"><i class="fas fa-users"></i></div>
                                Khách Hàng
                            </a>
                            <a class="nav-link <?= $product_active; ?>"  href="{{ route('products.index') }}">
                                <div class="sb-nav-link-icon"><i class="fas fa-box-open"></i></div>
                               Sản Phẩm
                            </a>
                            <a class="nav-link <?= $category_active; ?>"   href="{{ route('categories.index') }}">
                                <div class="sb-nav-link-icon"><i class="fab fa-accusoft"></i></div>
                                Loại sản phẩm
                            </a>
                            </a>
                            <a class="nav-link <?= $order_active; ?>"  href="{{ route('orders.index') }}">
                                <div class="sb-nav-link-icon"><i class="fas fa-file-invoice"></i>   </div>
                                Đơn đặt hàng
                            </a>
                            <a class="nav-link <?= $orderdetail_active; ?>"  href="{{ route('orderdetails.index') }}">
                                <div class="sb-nav-link-icon"><i class="fas fa-pencil-alt"></i>   </div>
                                Chi Tiết Đơn Hàng
                            </a>
                           
                    </div>
                   
                </nav>
            </div>