<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\ProductModel;

class OrderdetailModel extends Model
{
    use HasFactory;
    protected $table = 'orderdetails';
    protected $fillable = ['orders_id' , 'products_id' , 'quantity' , 'price'];
    public $timestamps = false;

    public function product() {
        return $this->belongsTo(ProductModel::class , 'products_id' , 'id');
    }
    public function order() {
        return $this->belongsTo(OrderModel::class , 'orders_id' , 'id');
    }
}
