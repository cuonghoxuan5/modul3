<?php

namespace App\Models;

use App\Http\Controllers\Admin\CategoryController;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProductModel extends Model
{
    use HasFactory;
    protected $table = 'products';
    protected $fillable = ['name' , 'price','produce','image','category_id'];
    public $timestamps = false;

    public function categories()
    {
        return $this->belongsTo(CategoryModel::class ,'category_id', 'id');
    }
    public function orderdetail()
    {
        return $this->belongsToMany(OrderdetailsModel::class ,'product_id', 'id');
    }

    
}
