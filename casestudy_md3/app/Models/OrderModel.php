<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OrderModel extends Model
{
    use HasFactory;
    protected $table = 'orders';
    protected $fillable = ['total' , 'status' , 'users_id','order_date'];
    public $timestamps = false;

    public function user()
    {
        return $this->belongsTo(User::class , 'users_id' , 'id');
    }
    
}
