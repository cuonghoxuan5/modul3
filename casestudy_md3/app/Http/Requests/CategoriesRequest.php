<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CategoriesRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'          => 'required|unique',
            'code'         => 'required|unique',
          
        ];
    }
    public function messages()
    {
        return [
            'name.required'         => 'Vui lòng nhập tên',
            'name.unique'         => 'Tên đã tồn tại',
            'code.required'        => 'Vui lòng nhập mã code',
            'code.unique'        => 'Mã code đã tồn tại'
           
        ];
    }
}
