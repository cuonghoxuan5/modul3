<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProductsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'          => 'required',
            'price'         => 'required',
            'image'         => 'required',
            'produce'       => 'required',
            'category_id'   => 'required',
        ];
    }
    public function messages()
    {   
        return [
            'name.required' => 'Vui lòng nhập tên',
            'price.required' => 'Vui lòng nhập giá',
            'image.required' => 'Vui lòng thêm ảnh',
            'produce.required' => 'Vui lòng nhập ngày sản xuất',
            'category_id.required' => 'Vui lòng nhập id hàng hóa'
        ];
    }
}
