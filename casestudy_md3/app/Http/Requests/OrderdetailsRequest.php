<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class OrderdetailsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'orders_id'      => 'required',
            'products_id'    => 'required',
            'quantity'      => 'required',
            'price'         => 'required',
        ];
    }
    public function messages()
    {
        return [
            'orders_id.required'      => 'Vui lòng nhập tên',
            'products_id.required'    => 'Vui lòng nhập tên sản phẩm',
            'quantity.required'      => 'Vui lòng nhập sô lượng sản phẩm',
            'price.required'         => 'Vui lòng nhập giá sản phẩm',
        ];
    }
}