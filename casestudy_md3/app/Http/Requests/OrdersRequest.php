<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class OrdersRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            
            'users_id'      => 'required',
            'total'         => 'required|max:11',
            'status'        => 'required',
            'order_date'    => 'required',
        ];
    }
    public function messages()
    {
        return [
            'users_id.required'     => 'Vui lòng nhập tên',
            'status.required'       => 'Vui lòng nhập trạng thái',
            'total.required'        => 'Vui lòng nhập giá tiền',
            'order_date.required'   => 'Vui lòng nhập ngày mua'
        ];
    }
}
