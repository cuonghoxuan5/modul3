<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UsersRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
           'name'               => 'required|min:3|max:50',
           'email'              => 'required|unique:users,email|email',
           'password'           => 'required|min:6|regex:/(^([a-z0-9]+)$)/u',
           'password_confirm'   => 'required|same:password',
           'date'               => 'required',  
           'gender'             => 'required',
           'address'            => 'required',
           'phone'              => 'required|unique:users,phone',
        //    'image'      => 'required'

        ];
    }
    public function messages()
    {   
        return [
            'name.required'         => "Vui lòng nhập tên",
            'name.min'              => "Vui lòng nhập ít nhất 3 ký tự",
            'name.max'              => "Vui lòng nhập không quá 50 ký tự",
            'email.required'        => 'Vui lòng nhập email',
            'email.unique'          => 'Email này đã tồn tại',
            'email.email'           => 'Đây không phải là email vui lòng nhập đúng email',
            'password.required'     => 'Vui lòng nhập mật khẩu',
            'password.min'          => 'Nhập ít nhát 6 kí tự',
            'password.regex'        => 'Nhập mật khẩu không đúng',
            'password_confirm.required'    => 'Vui lòng xác nhập lại mật khẩu',
            'password_confirm.same'        => ' Mật khẩu không trùng khớp',
            'date.required'         => 'Vui lòng nhập ngày tháng',
            'gender.required'       => 'Vui lòng chọn giới tính',
            'address.required'      => 'Vui lòng nhập địa chỉ',
            'phone.required'        => 'Không được bỏ trống trường này',
            'phone.unique'          => 'Số này đã tồn tại',
            // 'image.required'        => 'Trường này không được bỏ trống',
        ];
 
    }
}
