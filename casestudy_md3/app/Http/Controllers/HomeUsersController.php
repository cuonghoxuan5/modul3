<?php

namespace App\Http\Controllers;

use App\Models\Cart;
use App\Models\CategoryModel;
use App\Models\ProductModel;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class HomeUsersController extends Controller
{
    public function index()
    {
        $users = Auth::user();
        $products = ProductModel::all();
        // $categories = CategoryModel::all();
        $products = ProductModel::paginate(6);
        
        $params = [
            'products' => $products,
            'users'    => $users
            // 'categories' => $categories
        ];
        return view('homeUser.webUser', $params);
    }
   public function category($id)
   {
        $users      = Auth::user();
        $categories = CategoryModel::all();
        $products   = ProductModel::where('category_id','=',$id)->paginate(6);
        
        $params = [
            'categories'=> $categories,
            'products'  => $products,
            'users'     => $users
        ];
        return view('homeUser.categoryUser' , $params);
   }
   public function detail($id)
   {    $users          = Auth::user();
        $categories     =  CategoryModel::all();
        $new_products   = ProductModel::all();
        $products       = ProductModel::where('id','=',$id)->first();
        $highlights     = ProductModel::where('price','>','2000000')->orderBy('price','asc')->limit(8)->get();
        // dd($highlights);
        $params     = [
            'categories'    => $categories,
            'products'      => $products,
            'highlights'    => $highlights,
            'new_products'  => $new_products,
            'users'         => $users
        ];
        return view('homeUser.detailUser' , $params);
   }
    public function cart()
    {
        $users          = Auth::user();
        $categories = CategoryModel::all();
        $carts      = DB::table('carts')
            ->join('users', 'users.id', '=', 'carts.user_id')
            ->join('products', 'products.id', '=', 'carts.product_id')
            ->select('carts.id', 'products.name', 'products.image', 'carts.price', DB::raw('(carts.price * carts.quantity) as totalPrice'), 'carts.quantity')
            ->where('carts.user_id', '=', 13)
            ->get();
        $totalQuantity =   DB::table('carts')
            ->select('user_id', DB::raw('sum(carts.quantity) as totalQuantity'))
            ->where('carts.user_id', '=', 13)
            ->groupBy('carts.user_id')
            ->first();
           
            $totalPrice =   DB::table('carts')
            ->select('user_id', DB::raw('sum(carts.price * carts.quantity) as totalPrice'))
            ->where('carts.user_id', '=', 13)
            ->groupBy('carts.user_id')
            ->first();
            // dd( $totalPirce);
        $params = [
            'users'         => $users,
            'categories'    => $categories,
            "carts"         => $carts,
            "totalQuantity" =>$totalQuantity,
            "totalPrice"   =>$totalPrice,
            
        ];
        return view('homeUser.cartUser' , $params);
    }

   public function addToCartUser(Request $request, $id)
   {
    $product             = DB::table('products')->where('id', $id)->first();

    $cart                = new Cart();
    $cart->user_id       = 13;
    $cart->product_id    = $product->id;
    $cart->price         = $product->price;
    $cart->quantity      = $request->quantity;
    $cart->save();
    return redirect()->back();
   }

   public function deleteUser($id)
   {
       $cart  = Cart::find($id);
       $cart->delete();
       return redirect()->back();
   }



}
