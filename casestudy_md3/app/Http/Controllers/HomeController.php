<?php

namespace App\Http\Controllers;

use App\Http\Requests\HomeLoginRequest;
use App\Models\Cart;
use App\Models\CategoryModel;
use App\Models\ProductModel;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;

class HomeController extends Controller
{
    public function login()
    {
        return view('home.login');
    }
    public function register()
    {
        return view('home.register');
    }

    public function home()
    {
        $products = ProductModel::all();
        $products = ProductModel::paginate(6);

        $params = [
            'products' => $products,
        ];
        return view('home.web', $params);
    }

    public function handlelogin(HomeLoginRequest $request)
    {

        $loginUsers = [
            'email' => $request->email,
            'password' => $request->password,
            'level' => 0,

        ];
        $loginAdmin =
            [
                'email' => $request->email,
                'password' => $request->password,
                'level' => 1,

            ];
        if (Auth::attempt($loginUsers)) {
            return redirect()->route('index');
        }
        if (Auth::attempt($loginAdmin)) {
            return redirect()->route('users.index');
        } else {
            Session::flash('error_email', 'Email của bạn không tồn tại');
            Session::flash('error_password', 'Mật khẩu của bạn không tồn tại');
            return redirect()->back();
        }
    }

    public function handleRegister(Request $request)
    {
        $messages = [
            'name.required'         => "Vui lòng nhập tên",
            'name.unique'           => "Tên đã tồn tại",
            'name.min'              => "Vui lòng nhập ít nhất 3 ký tự",
            'name.max'              => "Vui lòng nhập không quá 50 ký tự",
            'email.required'        => 'Vui lòng nhập email',
            'email.unique'          => 'Email này đã tồn tại',
            'email.email'           => 'Đây không phải là email vui lòng nhập đúng email',
            'password.required'     => 'Vui lòng nhập mật khẩu',
            'password.min'          => 'Nhập ít nhát 6 kí tự',
            'password.regex'        => 'Nhập mật khẩu không đúng',
            'password_confirm.required'    => 'Vui lòng xác nhập lại mật khẩu',
            'password_confirm.same'        => ' Mật khẩu không trùng khớp',
            'date.required'         => 'Vui lòng nhập ngày tháng',
            'gender.required'       => 'Vui lòng chọn giới tính',
            'address.required'      => 'Vui lòng nhập địa chỉ',
            'phone.required'        => 'Không được bỏ trống trường này',
            'phone.unique'          => 'Số này đã tồn tại',
        ];
        $roles = [
            'name'               => 'required|unique:users,name|min:3|max:50',
            'email'              => 'required|unique:users,email|email',
            'password'           => 'required|min:6|regex:/(^([a-z0-9]+)$)/u',
            'password_confirm'   => 'required|same:password',
            'date'               => 'required',
            'gender'             => 'required',
            'address'            => 'required',
            'phone'              => 'required|unique:users,phone',
        ];
        $this->validate($request, $roles, $messages);

        $user = new User();
        $user->name     = $request->name;
        $user->email    = $request->email;
        $user->password = Hash::make($request->password);
        // $user->password_confirm = $request->password_confirm;
        $user->date     = $request->date;
        $user->gender   = $request->gender;
        $user->address  = $request->address;
        $user->phone    = $request->phone;
        $user->level    = 0;
        $user->save();
        Session::flash('success', 'Đăng ký thành công');
        return redirect()->route('home.login');
    }
    public function web()
    {

        return view('home.web');
    }
    public function detail($id)
    {
        $categories     =  CategoryModel::all();
        $new_products   = ProductModel::all();
        $products       = ProductModel::where('id', '=', $id)->first();
        $highlights     = ProductModel::where('price', '>', '2000000')->orderBy('price', 'asc')->limit(8)->get();
        // dd($highlights);
        $params         = [
            'categories'    => $categories,
            'products'      => $products,
            'highlights'    => $highlights,
            'new_products'  => $new_products
        ];
        return view('home.shop-detail', $params);
    }
    public function category($id)
    {
        $categories    = CategoryModel::all();
        $products      = ProductModel::where('category_id', '=', $id)->paginate(6);

        $params = [
            'categories' => $categories,
            'products' => $products
        ];
        return view('home.category', $params);
    }

    public function cart()
    {
        // $products   = ProductModel::where('id','=')->first();
        $categories = CategoryModel::all();
        $carts      = DB::table('carts')
            ->join('users', 'users.id', '=', 'carts.user_id')
            ->join('products', 'products.id', '=', 'carts.product_id')
            ->select('carts.id', 'products.name', 'products.image', 'carts.price', DB::raw('(carts.price * carts.quantity) as totalPrice'), 'carts.quantity')
            ->where('carts.user_id', '=', 13)
            ->get();
        $totalQuantity =   DB::table('carts')
            ->select('user_id', DB::raw('sum(carts.quantity) as totalQuantity'))
            ->where('carts.user_id', '=', 13)
            ->groupBy('carts.user_id')
            ->first();
           
        $totalPrice =   DB::table('carts')
            ->select('user_id', DB::raw('sum(carts.price * carts.quantity) as totalPrice'))
            ->where('carts.user_id', '=', 13)
            ->groupBy('carts.user_id')
            ->first();
            // dd( $totalPirce);
        $params = [
            
            'categories'    => $categories,
            "carts"         => $carts,
            "totalQuantity" =>$totalQuantity,
            "totalPrice"   =>$totalPrice,
            
        ];
        return view('home.cart', $params);
    }
    // Thêm vào giỏ hàng 
    public function addToCart(Request $request, $id)
    {
        $product             = DB::table('products')->where('id', $id)->first();

        $cart                = new Cart();
        $cart->user_id       = 13;
        $cart->product_id    = $product->id;
        $cart->price         = $product->price;
        $cart->quantity      = $request->quantity;
        $cart->save();
        return redirect()->back();
    }
    // Xóa 
    public function delete($id)
    {
        $cart  = Cart::find($id);
        $cart->delete();
        return redirect()->back();
    }
}
