<?php

namespace App\Http\Controllers\Admin;
use App\Models\ProductModel;
use App\Http\Controllers\Controller;
use App\Http\Requests\ProductsRequest;
use App\Models\CategoryModel;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $search = $request->search;
        
        // mục tìm kiếm 
        if ($search) {
            $products = ProductModel::where('name','like','%'.$search.'%')->paginate(5);
        } else {
            $products = ProductModel::orderBy('name','asc')->paginate(5);
        }
        $params = [
            'products' => $products
        ];
        return view('admin.products.index', $params);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {   
        $categories = CategoryModel::all();
        return view ('admin.products.create',compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProductsRequest $request)
    {
        $products = new ProductModel();
        $products->name         = $request->name;
        $products->price        = $request->price;

        if ($request->hasFile('image')) {
            // tao 1 bien tro toi image 
            $get_image          = $request->image;
            // tao file dung anh 
            $path               = 'public/upload/';

            $get_name_image     = $get_image->getClientOriginalName();
            $name_image         = current(explode('.', $get_name_image));
            $new_image          = $name_image . rand(0, 99) . '.' . $get_image->getClientOriginalExtension();
            $get_image->move($path, $new_image);
            $products->image    = $new_image;
        }
        $products->produce      = $request->produce;
        $products->category_id  = $request->category_id;
        
        // $users->image    = $request->image;
        // dd($request->hasFile('image'));
      
        $products->save();

        //dung session de dua ra thong bao
        Session::flash('success', 'Tạo mới khách hàng thành công');
        //tao moi xong quay ve trang danh sach khach hang
        return redirect()->route('products.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $products = ProductModel::find($id);
        return view('admin.products.show', compact('products'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $products = ProductModel::find($id);
        $categories = CategoryModel::all();
        $params = [
            'products' => $products,
            'categories' => $categories
        ];
        return view('admin.products.edit', $params);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ProductsRequest $request, $id)
    {
       $products =  ProductModel::find($id);
       $new_image = $products->image;
        if ($request->hasFile('image')) {
            // tao 1 bien tro toi image 
            $get_image          = $request->image;
            // tao file dung anh 
            $path               = 'public/upload/';
            $get_name_image     = $get_image->getClientOriginalName();
            $name_image         = current(explode('.', $get_name_image));
            $new_image          = $name_image . rand(0, 99) . '.' . $get_image->getClientOriginalExtension();
            $get_image->move($path, $new_image);
            // $products->image    = $new_image;
        }
        // User::find($id)->update($request->only('name', 'status'));
        $products->name         = $request->name;
        $products->price        = $request->price;
        $products->produce      = $request->produce;
        $products->category_id  = $request->category_id;
        $products->image        = $new_image;
        $products->save();
        //tao moi xong quay ve trang danh sach khach hang
        return redirect()->route('products.index')->with('success', 'Cập nhật thành công.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try{
            ProductModel::find($id)->delete();
        }catch(Exception $e){
            Session::flash('error',' Xóa không thành công');
            return back()->withError('Xóa không thành công')->withInput();
        }
        
        return redirect()->route('products.index')->with('success', 'Xóa thành công');
    }
}
