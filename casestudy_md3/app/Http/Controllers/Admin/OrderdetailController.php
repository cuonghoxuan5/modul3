<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\OrderdetailsRequest;
use App\Models\OrderdetailModel;
use App\Models\OrderModel;
use App\Models\ProductModel;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class OrderdetailController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $search = $request->search;
        
        // mục tìm kiếm 
        if ($search) {
            $orderdetails = OrderdetailModel::where('orders_id','like','%'.$search.'%')->paginate(5);
        } else {
            $orderdetails = OrderdetailModel::orderBy('orders_id','asc')->paginate(5);
        }
        $params = [
            'orderdetails' => $orderdetails
        ];  
        // echo '<pre>';
        // print_r($orderdetails);
        // echo '</pre>';
        return view('admin.orderdetails.index', $params);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $orders     = OrderModel::all();
        $products   = ProductModel::all();
        $params     = [
            'orders'    => $orders,
            'products'  => $products,
        ];
        return view('admin.orderdetails.create' ,$params );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(OrderdetailsRequest $request)
    {
        $orderdetails                 = new OrderdetailModel();
        // dd($orderdetails);
        $orderdetails->orders_id      = $request->orders_id;
        $orderdetails->products_id    = $request->products_id;
        $orderdetails->quantity       = $request->quantity;
        $orderdetails->price          = $request->price;
        $orderdetails->save();
        Session::flash('success', 'Tạo mới khách hàng thành công');
        //tao moi xong quay ve trang danh sach khach hang
        return redirect()->route('orderdetails.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $orderdetails = OrderdetailModel::find($id);
        return view('admin.orderdetails.show', compact('orderdetails'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $orderdetails   = OrderdetailModel::find($id);
        $orders         = OrderModel::all();
        $products       = ProductModel::all();
        $params = [
            'orderdetails'  => $orderdetails,
            'orders'        => $orders,
            'products'      => $products
        ];
        return view('admin.orderdetails.edit' , $params);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(OrderdetailsRequest $request, $id)
    {
        OrderdetailModel::find($id)->update($request->only('orders_id','products_id','quantity','price'));
        return redirect()->route('orderdetails.index')->with('success', 'Cập nhật thành công.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try{
            OrderdetailModel::find($id)->delete();
           }
       catch(Exception $e)
           {
               Session::flash('errors','Xóa không thành công');
               return back()->withError('Xóa không thành công')->withInput();
           }
        // OrderdetailModel::find($id)->delete();
        return redirect()->route('orderdetails.index')->with('success', 'Xóa thành công');
    }
}



