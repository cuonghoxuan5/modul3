<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\CategoryModel;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $search = $request->search;
        
        // mục tìm kiếm 
        if ($search) {
            $categories = CategoryModel::where('name','like','%'.$search.'%')->paginate(5);
        } else {
            $categories = CategoryModel::orderBy('name','asc')->paginate(5);
        }
        $params = [
            'categories' => $categories
        ];

        return view('admin.categories.index', $params);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = CategoryModel::all();
        return view ('admin.categories.create',compact('categories'));
    }
    

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $categories          = new CategoryModel();
        $categories->name    = $request->name;
        $categories->code    = $request->code;
      
        $categories->save();

        //dung session de dua ra thong bao
        Session::flash('success', 'Tạo mới khách hàng thành công');
        //tao moi xong quay ve trang danh sach khach hang
        return redirect()->route('categories.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $categories = CategoryModel::find($id);
        return view('admin.categories.show', compact('categories'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $categories = CategoryModel::find($id);
        $params = [
            'categories' => $categories
        ];
        return view('admin.categories.edit', $params);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
         // User::find($id)->update($request->only('name', 'status'));
         CategoryModel::find($id)->update($request->only('name','code'));

         //tao moi xong quay ve trang danh sach khach hang
         return redirect()->route('categories.index')->with('success', 'Cập nhật thành công.');
     }
    

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try{
            CategoryModel::find($id)->delete();
           }
       catch(Exception $e)
           {
               Session::flash('errors','Xóa không thành công');
               return back()->withError('Xóa không thành công')->withInput();
           }
        // CategoryModel::find($id)->delete();
        return redirect()->route('categories.index')->with('success', 'Xóa thành công');
    }
}
