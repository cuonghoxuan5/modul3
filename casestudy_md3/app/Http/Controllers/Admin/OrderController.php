<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\OrdersRequest;
use App\Models\OrderModel;
use App\Models\User;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
       
        $search = $request->search;
        
        // mục tìm kiếm 
        if ($search) {
            $orders = OrderModel::where('users_id','like','%'.$search.'%')->paginate(5);
        } else {
            $orders = OrderModel::orderBy('users_id','asc')->paginate(5);
        }
        $params = [
            'orders' => $orders
        ];
        return view ('admin.orders.index',$params );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $users = User::all();
        return view('admin.orders.create' ,compact('users'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(OrdersRequest $request)
    {   
        $orders                 = new OrderModel();
        $orders->users_id       = $request->users_id;
        $orders->total          = $request->total;
        $orders->order_date     = $request->order_date;
        $orders->status         = $request->status;

        $orders->save();
        Session::flash('success', 'Tạo mới khách hàng thành công');
        //tao moi xong quay ve trang danh sach khach hang
        return redirect()->route('orders.index');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $orders = OrderModel::find($id);
        return view('admin.orders.show', compact('orders'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $order = OrderModel::find($id);
        $users = User::all();
        $params = [
            'order' => $order,
            'users' => $users
        ];
        return view('admin.orders.edit' , $params);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(OrdersRequest $request, $id)
    {
        
        OrderModel::find($id)->update($request->only('users_id','total','order_date','status'));
        return redirect()->route('orders.index')->with('success', 'Cập nhật thành công.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try{
            OrderModel::find($id)->delete();
           }
       catch(Exception $e)
           {
               Session::flash('errors','Xóa không thành công');
               return back()->withError('Xóa không thành công')->withInput();
           }
        // OrderModel::find($id)->delete();
        return redirect()->route('orders.index')->with('success', 'Xóa thành công');
    }
}
