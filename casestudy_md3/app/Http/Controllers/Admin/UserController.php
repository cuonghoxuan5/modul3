<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\UsersRequest;
use App\Models\User;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $search = $request->search;
        
        // mục tìm kiếm 
        if ($search) {
            $users = User::where('name','like','%'.$search.'%')->paginate(5);
        } else {
            $users = User::orderBy('name','asc')->paginate(5);
        }
        $params = [
            'users' => $users
        ];
        return view('admin.users.index', $params);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.users.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UsersRequest $request)
    {
        $user = new User();
        $user->name     = $request->name;
        $user->email    = $request->email;
        $user->password = Hash::make($request->password);
        $user->date     = $request->date;
        $user->gender   = $request->gender;
        $user->address  = $request->address;
        $user->phone    = $request->phone;
        $user->level    = 0;
        // $users->image    = $request->image;
        // dd($request->hasFile('image'));
        // if ($request->hasFile('image')) {
        //     $get_image          = $request->image;
        //     $path               = 'public/upload/';
        //     $get_name_image     = $get_image->getClientOriginalName();
        //     $name_image         = current(explode('.', $get_name_image));
        //     $new_image          = $name_image . rand(0, 99) . '.' . $get_image->getClientOriginalExtension();
        //     $get_image->move($path, $new_image);
        //     $user->image       = $new_image;
        // }
        $user->save();

        //dung session de dua ra thong bao
        Session::flash('success', 'Tạo mới khách hàng thành công');
        //tao moi xong quay ve trang danh sach khach hang
        return redirect()->route('users.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::find($id);
        return view('admin.users.show', compact('user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::find($id);
        $params = [
            'user' => $user
        ];
        return view('admin.users.edit', $params);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UsersRequest $request, $id)
    {
        // User::find($id)->update($request->only('name', 'status'));
        User::find($id)->update($request->only('name', 'email','password','password_confirm', 'date', 'gender', 'address', 'phone', 'image'));

        //tao moi xong quay ve trang danh sach khach hang
        return redirect()->route('users.index')->with('success', 'Cập nhật thành công.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try{
             User::find($id)->delete();
            }
        catch(Exception $e)
            {
                Session::flash('errors','Xóa không thành công');
                return back()->withError('Xóa không thành công')->withInput();
            }
            return redirect()->back()->with('success', 'Xóa thành công');
    }
}