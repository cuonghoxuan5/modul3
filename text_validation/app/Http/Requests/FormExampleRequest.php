<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class FormExampleRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
           'name'=> 'required|min:2|max:30',
           'age' => 'required|numeric|min:18',
        ];
    }
    public function messages()
    {
        $messages = [
            'name.required' => 'name không được bỏ trống',
            'name.min'      => 'nhập ít nhất 2 kí tự',
            'name.max'      => ' nhập tối đa 30 kí tự',
            'age.required'  => 'tuổi không được bỏ trống !',
            'age.numeric'   => 'trường này bắt buộc nhập bằng số',
            'age.min'       => 'phải trên 18t',
    ];
        return $messages;
    }
   
}